<!DOCTYPE html>
<html>
<head>
	<title>FLETNIX: Account</title>
	<link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body>
	<header>
		<?php
		require 'functions.php';
		$page = 'account';
        include 'header.php';
		?>
	</header>
    <main class="container">
        <?php
        if (array_key_exists('gebruikersnaam',$_SESSION)) {
        ?>
            <h1>Mijn profiel</h1>
            <div id="profiel">
                <div class="column">
                    <h3>Naam</h3>
                    <p><?php echo $_SESSION['gebruiker_gegevens']['Name']; ?></p>
                    <h3>Gebruikersnaam</h3>
                    <p><?php echo $_SESSION['gebruiker_gegevens']['Customer_Mail_Address']; ?></p>
                    <h3>Land</h3>
                    <p><?php echo $_SESSION['gebruiker_gegevens']['Country_Name']; ?></p>
                </div>
                <div class="column">
                    <h3>Paypal account</h3>
                    <p><?php echo $_SESSION['gebruiker_gegevens']['Paypal_Account']; ?></p>
                    <h3>Startdatum abonnement</h3>
                    <p><?php echo $_SESSION['gebruiker_gegevens']['Subscription_Start']; ?></p>
                    <h3>Einddatum abonnement</h3>
                    <p><?php
                        if(isset($_SESSION['gebruiker_gegevens']['Subscription_End']))
                        {
                            echo $_SESSION['gebruiker_gegevens']['Subscription_End'];
                        } else {
                            echo "Niet opgegeven";
                        } ?></p>
                </div>
            </div>
        <?php } else {
            echo "<h1>Profiel</h1>";
            echo "<p>Je moet <a href=\"login.php\">inloggen</a> om deze pagina te kunnen bekijken.</p>";
        } ?>
    </main>
    <footer>
		<?php include 'footer.php'; ?>
	</footer>
</body>
</html>