<?php

function connect_to_database(){
	$hostname = "localhost"; //Naam van de Server
	$dbname = "fletnix";    //Naam van de Database
	$username = "sa";      //Inlognaam
	$pw = "esterade";      //Password

try {
	global $pdo;
	$pdo = new PDO ("sqlsrv:Server=$hostname;Database=$dbname", "$username", "$pw");
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
	echo $e->getMessage();
	die('Database connection error');
}
}

function get_movies():array{
	global $pdo;
	try{
		$movie_data = array();
		$data=$pdo->query("SELECT M.Movie_id,M.Title,M.Cover_Image FROM Movie M");

		while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
			array_push($movie_data, $row);
		}
		return $movie_data;
	} catch (Exception $e) {
		echo $e->getMessage();
		return array();
	}
}

function get_movie_info(int $movieid):array{
	global $pdo;
	try{
		$movie_info = array();
		$data=$pdo->query("SELECT M.Title,M.Duration,M.Description,M.Publication_Year,M.Cover_Image,M.URL FROM Movie M WHERE Movie_id = $movieid");
	} catch (Exception $e) {
		echo $e->getMessage();
		return array();
	}
}

function search_movie(string $search):array{
	global $pdo;
	try{
		$search_result = array();
		$data=$pdo->query("SELECT M.Movie,M.Title,M.Cover_Image FROM Movie M WHERE M.Title LIKE '%$search%' ");
	} 
} catch (Exception $e) {
		echo $e->getMessage();
		return array();
	}
}
