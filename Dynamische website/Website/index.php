<!DOCTYPE html>
<html>
<head>
	<title>FLETNIX: Homepagina</title>
	<link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
	<?php
	require 'functions.php';
	session_start();
	session_destroy();
	?>
</head>
<body>
	<header id="banner">
		<img class="background" alt="background" src="images/fantasticbeastsbigstill-gradient.jpg">
		<div id="content">
			<div class="container">
                <div class="top">
                    <a href="index.php">
                    	<img class="logo" alt="Logo" src="images/fletnix-logo.png">
                    </a>
                </div>
				<div id="header_button">
					<h1>Bekijk nu de nieuwste films en series!</h1>
                    <a href="login.php" class="button">inloggen</a>
				    <a href="#registration_scroll" class="button">registreren</a>
				</div>
			</div>
		</div>
	</header>
	<section id="over_ons">
		<div class="container">
			<h2>Over FLET<strong>NIX</strong></h2>
			<div class="one_half">
				<p>Fletnix is een nieuw video-on-demand internetplatform. Wij bieden aangekochte &eacute;n zelf geproduceerde films en series voor alle leeftijden.</p>
				<p>Bij ons betaal je een vast bedrag per maand om onbeperkt films en series te bekijken. Wat je kunt bekijken en in welke kwaliteit hangt af van <a href="registreren.php">welk pakket je kiest</a>.</p>
			</div><div class="one_half last">
				<p>Ons basis pakket is voor klanten die niet te veel geld uit willen geven. Je kunt hiermee alle content bekijken, behalve het door ons zelf geproduceerde materiaal. Dit abonnement is per jaar opzegbaar.</p>
				<p>Wil je dit in HD kunnen bekijken, hebben we het standaard pakket voor je. Deze is per maand opzegbaar. Voor de echte Fletnix fans hebben we ook het premium pakket, waarbij je ook onze originals kunt bekijken!</p>
			</div>
		</div>
	</section>
	<section id="registration">
        <a id="registration_scroll"></a>
		<div class="container">
			<h2>Registreren voor nieuwe klanten</h2>
			<p>Kies een van onze pakketten.</p>
			<div id="wrapper">
				<div id="pakket_1" class="pakketten">
					<h3>Basic</h3>
                    <ul>
                        <li>Onbeperkt streamen</li>
                        <li>Per jaar opzegbaar</li>
                        <li class="price">&euro;7,50 p.m.</li>
                    </ul>
                    <a href="registreren.php">registreer</a>
				</div>
				<div id="pakket_2" class="pakketten">
					<h3>Standaard</h3>
                    <ul>
                        <li>Alles in Full HD</li>
                        <li>Per maand opzegbaar</li>
                        <li class="price">&euro;9,50 p.m.</li>
                    </ul>
                    <a href="registreren.php">registreer</a>
				</div>
				<div id="pakket_3" class="pakketten">
					<h3>Premium</h3>
                    <ul>
                        <li>Extra Fletnix content</li>
                        <li>Per maand opzegbaar</li>
                        <li class="price">&euro;11,00 p.m.</li>
                    </ul>
                    <a href="registreren.php">registreer</a>
				</div>
			</div>
		</div>
	</section>
    <footer>
		<?php include 'footer.php'; ?>
	</footer>
</body>
</html>