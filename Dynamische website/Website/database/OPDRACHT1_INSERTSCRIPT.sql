DELETE FROM Movie_Director
DELETE FROM Movie_Cast
DELETE FROM Movie_Genre
DELETE FROM Genre
DELETE FROM WatchHistory
DELETE FROM Customer
DELETE FROM Country
DELETE FROM Person
DELETE FROM Movie

INSERT INTO Person
VALUES	(1, 'Wachowski', 'Lana',  'F'),
		(2,'Wachowski', 'Lilly', 'F'),
		(3,'Reeves','Keanu','M'),
		(4,'Fishburne','Laurence','M'),
		(5,'Moss','Carrie-Anne','F'),
		(6,'Coppola','Francis Ford','M'),
		(7,'Brando','Marlon','M'),
		(8,'Pacino','Al','M'),
		(9,'Caan','James','M'),
		(10,'De Niro','Robert','M'),
		(11,'Duvall','Robert','M'),
		(12,'Nolan','Christopher','M'),
		(13,'Bale','Christian','M'),
		(14,'Ledger','Heath','M'),
		(15,'Eckhart','Aaron','M'),
		(16,'Lumet','Sidney','M'),
		(17,'Fonda','Henry','M'),
		(18,'Cobb','Lee J.','M'),
		(19,'Balsam','Martin','M'),
		(20,'Tarantino','Quentin','M'),
		(21,'Travolta','John','M'),
		(22,'Thurman','Uma','F'),
		(23,'Jackson','Samuel L.','M'),
		(24,'Hardy','Tom','M'),
		(25,'Hathaway','Anne','F'),
		(26,'DiCaprio','Leonardo','M'),
		(27,'Gordon-Levitt','Joseph','M'), 
		(28,'Page','Ellen','F'),
		(29,'Aronofsky','Darren','M'),
		(30,'Portman','Natalie','F'),
		(31,'Kunis','Mila','F'),
		(32,'Cassel','Vincent','M'),
		(33,'Lucas','Jon','M'),
		(34,'Moore','Scott','M'),
		(35,'Hahn','Kathryn','F'),
		(36,'Bell','Kristen','F'),
		(37,'Miller','Tim','M'),
		(38,'Reynolds','Ryan','M'),
		(39,'Baccarin','Morena','F'),
		(40,'Miller','T.J.','M'),
		(41,'Derrickson','Scott','M'),
		(42,'Cumberbatch','Benedict','M'),
		(43,'Ejiofor','Chiwetel','M'),
		(44,'McAdams','Rachel','F'),
		(45,'Yates','David','M'),
		(46,'Redmayne','Eddie','M'),
		(47,'Waterston','Katherine','F'),
		(48,'Sudol','Alison','F'),
		(49,'McConaughey','Matthew','M'),
		(50,'Chastain','Jessica','F'),
		(51,'Greengrass','Paul','M'),
		(52,'Damon','Matt','M'),
		(53,'Lee Jones','Tommy','M'),
		(54,'Vikander','Alicia','F'),
		(55,'Spielberg','Steven','M'),
		(56,'Day-Lewis','Daniel','M'),
		(57,'Field','Sally','F'),
		(58,'Strathairn','David','M'),
		(59,'Scorsese','Martin','M'),
		(60,'Mortimer','Emily','F'),
		(61,'Ruffalo','Mark','M'),
		(62,'Jackson','Peter','M'),
		(63,'Freeman','Martin','M'),
		(64,'McKellen','Ian','M'),
		(65,'Armitage','Richard','M'),
		(66,'Scott','Ridley','M'),
		(67,'Wiig','Kristen','F'),
		(68,'I��rritu','Alejandro G.','M'),
		(69,'Poulter','Will','M'),
		(70, 'Emmerich', 'Ronald', 'M'),
		(71, 'Hemsworth', 'Liam', 'M'),
		(72, 'Goldblum', 'Jeff', 'M'),
		(73, 'Carloni', 'Alessandro', 'M'),
		(74, 'Yuh Nelson', 'Jennifer', 'F'),
		(75, 'Black', 'Jack', 'M'),
		(76, 'Cranston', 'Bryan', 'M'),
		(77, 'Jolie', 'Angelina', 'F'),
		(78, 'Osborn', 'Mark', 'M'),
		(79, 'Stevenson', 'John', 'M'),
		(80, 'Carr', 'Steve', 'M'),
		(81, 'James', 'Kevin', 'M'),
		(82, 'Howard', 'Byron', 'M'),
		(83, 'Moore', 'Rich', 'M'),
		(84, 'Goodwin', 'Ginnifer', 'F'),
		(85, 'Bateman', 'Jason', 'M'),
		(86, 'Elba', 'Idris', 'M'),
		(87, 'Reed', 'Peyton', 'M'),
		(88, 'Rudd', 'Paul', 'M'),
		(89, 'Douglas', 'Michael', 'M'),
		(90, 'Lilly', 'Evangeline', 'F'),
		(91, 'Favreau', 'Jon', 'M'),
		(92, 'Downey Jr.', 'Robert', 'M'),
		(93, 'Howard', 'Terrence', 'M'),
		(94, 'Paltrow', 'Gwyneth', 'F'),
		(95, 'Cheadle', 'Don', 'M'),
		(96, 'Black', 'Shane', 'M'),
		(97, 'Gunn', 'James', 'M'),
		(98, 'Pratt', 'Chris', 'M'),
		(99, 'Diesel', 'Vin', 'M'),
		(100, 'Cooper', 'Bradley', 'M'),
		(101, 'Yates', 'David', 'M'),
		(102, 'Radcliffe', 'Daniel', 'M'), 
		(103, 'Watson', 'Emma', 'F'),
		(104, 'Grint', 'Rupert', 'M')

INSERT INTO Movie 
VALUES	(11, 'Matrix, The', 122, 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.', 1999,'the_matrix', NULL, 2.50, 'https://www.youtube-nocookie.com/embed/m8e-FF8MsqU?rel=0&amp;showinfo=0'),
		(12, 'Matrix Reloaded, The', 106, 'Neo and the rebel leaders estimate that they have 72 hours until 250,000 probes discover Zion and destroy it and its inhabitants. During this, Neo must decide how he can save Trinity from a dark fate in his dreams.', 2003, 'the_matrix_reloaded', 11, 2.50, 'https://www.youtube-nocookie.com/embed/kYzz0FSgpSU?rel=0&amp;showinfo=0'),
		(13, 'Matrix Revolutions, The', 103, 'The human city of Zion defends itself against the massive invasion of the machines as Neo fights to end the war at another front while also opposing the rogue Agent Smith.', 2003, 'the_matrix_revolutions', 12, 2.50, 'https://www.youtube-nocookie.com/embed/hMbexEPAOQI?rel=0&amp;showinfo=0'),
		(14, 'Godfather, The', 175, 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.', 1972, 'the_godfather', NULL, 2.00, 'https://www.youtube-nocookie.com/embed/sY1S34973zA?rel=0&amp;showinfo=0'),
		(15, 'Godfather: Part II, The', 202, 'The early life and career of Vito Corleone in 1920s New York is portrayed while his son, Michael, expands and tightens his grip on his crime syndicate stretching from Lake Tahoe, Nevada to pre-revolution 1958 Cuba.', 1974,'the_godfather_part_II', 14, 2.20, 'https://www.youtube-nocookie.com/embed/9O1Iy9od7-A?rel=0&amp;showinfo=0'),
		(16, 'Dark Knight, The', 152, 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.', 2008, 'the_dark_knight', NULL, 2.50, 'https://www.youtube-nocookie.com/embed/EXeTwQWrcwY?rel=0&amp;showinfo=0'),
		(17, '12 Angry Men', 96, 'A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.', 1957, '12_angry_men', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/fSG38tk6TpI?rel=0&amp;showinfo=0'),
		(18, 'Pulp Fiction',154,'The lives of two mob hit men, a boxer, a gangster''s wife, and a pair of diner bandits intertwine in four tales of violence and redemption.',1994,'pulp_fiction',NULL,1.50,'https://www.youtube-nocookie.com/embed/ewlwcEBTvcg?rel=0&amp;showinfo=0'),
		(19, 'The Dark Knight Rises', 164,'Eight years after the Joker''s reign of anarchy, the Dark Knight, with the help of the enigmatic Selina, is forced from his imposed exile to save Gotham City, now on the edge of total annihilation, from the brutal guerrilla terrorist Bane.', 2012,'the_dark_knight_rises',16,0.50,'https://www.youtube-nocookie.com/embed/z5Humz3ONgk?rel=0&amp;showinfo=0'),
		(20, 'Inception',148,'A thief, who steals corporate secrets through use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.',2010,'inception',NULL,1.50,'https://www.youtube-nocookie.com/embed/YoHD9XEInc0?rel=0&amp;showinfo=0'),
		(21, 'Black Swan',108,'A committed dancer wins the lead role in a production of Tchaikovsky''s "Swan Lake" only to find herself struggling to maintain her sanity.',2010,'black_swan',NULL,1.50,'https://www.youtube-nocookie.com/embed/coVi98u5RL0?rel=0&amp;showinfo=0'),
		(22, 'Bad Moms',100, 'When three overworked and under-appreciated moms are pushed beyond their limits, they ditch their conventional responsibilities for a jolt of long overdue freedom, fun, and comedic self-indulgence.',2016,'bad_moms',NULL,1.50,'https://www.youtube-nocookie.com/embed/P0FNjPsANGk?rel=0&amp;showinfo=0'),
		(23, 'Deadpool', 108,'A fast-talking mercenary with a morbid sense of humor is subjected to a rogue experiment that leaves him with accelerated healing powers and a quest for revenge.',2016,'deadpool',NULL,1.50,'https://www.youtube-nocookie.com/embed/FyKWUTwSYAs?rel=0&amp;showinfo=0'),
		(24, 'Doctor Strange',115,'A former neurosurgeon embarks on a journey of healing only to be drawn into the world of the mystic arts.',2016,'doctor_strange',NULL,1.50,'https://www.youtube-nocookie.com/embed/Lt-U_t2pUHI?rel=0&amp;showinfo=0'),
		(25, 'Fantastic Beasts and Where to Find Them',133,'The adventures of writer Newt Scamander in New York''s secret community of witches and wizards seventy years before Harry Potter reads his book in school.',2016,'fantastic_beasts',NULL,1.50,'https://www.youtube-nocookie.com/embed/YdgQj7xcDJo?rel=0&amp;showinfo=0'),
		(26, 'Interstellar',169,'A team of explorers travel through a wormhole in space in an attempt to ensure humanity''s survival.',2014,'interstellar',NULL,1.50,'https://www.youtube-nocookie.com/embed/zSWdZVtXT7E?rel=0&amp;showinfo=0'),
		(27, 'Jason Bourne',123,'The CIA''s most dangerous former operative is drawn out of hiding to uncover more explosive truths about his past.',2016,'jason_bourne',NULL,1.50,'https://www.youtube-nocookie.com/embed/F4gJsKZvqE4?rel=0&amp;showinfo=0'),
		(28, 'Lincoln',150,'As the Civil War continues to rage, America''s president struggles with continuing carnage on the battlefield as he fights with many inside his own cabinet on the decision to emancipate the slaves.',2012,'lincoln',NULL,1.50,'https://www.youtube-nocookie.com/embed/qiSAbAuLhqs?rel=0&amp;showinfo=0'),
		(29, 'Shutter Island',138,'In 1954, a U.S. marshal investigates the disappearance of a murderess who escaped from a hospital for the criminally insane.',2010,'shutter_island',NULL,1.50,'https://www.youtube-nocookie.com/embed/qdPw9x9h5CY?rel=0&amp;showinfo=0'),
		(30, 'The Hobbit: An Unexpected Journey',169,'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.',2012,'the_hobbit',NULL,1.50,'https://www.youtube-nocookie.com/embed/SDnYMbYB-nU?rel=0&amp;showinfo=0'),
		(31, 'The Martian',144,'An astronaut becomes stranded on Mars after his team assume him dead, and must rely on his ingenuity to find a way to signal to Earth that he is alive.',2015,'the_martian',NULL,1.50,'https://www.youtube-nocookie.com/embed/ej3ioOneTy8?rel=0&amp;showinfo=0'),
		(32, 'The Revenant',156,'A frontiersman on a fur trading expedition in the 1820s fights for survival after being mauled by a bear and left for dead by members of his own hunting team.',2015,'the_revenant',NULL,1.50,'https://www.youtube-nocookie.com/embed/LoebZZ8K5N0?rel=0&amp;showinfo=0'),
		--Arjan
		(33, 'Independence Day: Resurgence', 119, 'We always knew they were coming back. Using recovered alien technology, the nations of Earth have collaborated on an immense defense program to protect the planet. But nothing can prepare us for the aliens� advanced and unprecedented force. Only the ingenuity of a few brave men and women can bring our world back from the brink of extinction.',2016,'independence_day_resurgence', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/LbduDRH2m2M?rel=0&amp;showinfo=0'),
		(34, 'Kung Fu Panda 3', 94, 'Continuing his "legendary adventures of awesomeness", Po must face two hugely epic, but different threats: one supernatural and the other a little closer to his home.',2016,'kung_fu_panda_3', 35, 1.50, 'https://www.youtube-nocookie.com/embed/10r9ozshGVE?rel=0&amp;showinfo=0'),
		(35, 'Kung Fu Panda 2', 90, 'Po is now living his dream as The Dragon Warrior, protecting the Valley of Peace alongside his friends and fellow kung fu masters, The Furious Five - Tigress, Crane, Mantis, Viper and Monkey. But Po�s new life of awesomeness is threatened by the emergence of a formidable villain, who plans to use a secret, unstoppable weapon to conquer China and destroy kung fu.',2011,'kung_fu_panda_2', 36, 1.50, 'https://www.youtube-nocookie.com/embed/FQ63rqSRrEI?rel=0&amp;showinfo=0'),
		(36, 'Kung Fu Panda', 92, 'When the Valley of Peace is threatened, lazy Po the panda discovers his destiny as the "chosen one" and trains to become a kung fu hero, but transforming the unsleek slacker into a brave warrior won''t be easy. It''s up to Master Shifu and the Furious Five -- Tigress, Crane, Mantis, Viper and Monkey -- to give it a try.',2008,'kung_fu_panda', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/PXi3Mv6KMzY?rel=0&amp;showinfo=0'),
		(37, 'Paul Blart: Mall Cop', 91, 'When a shopping mall is taken over by a gang of organized crooks, it''s up to a mild-mannered security guard to save the day.',2009,'paul_blart', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/E-fGUgMK9bI?rel=0&amp;showinfo=0'),
		(38, 'Zootopia', 108, 'In the animal city of Zootopia, a fast-talking fox who''s trying to make it big goes on the run when he''s framed for a crime he didn''t commit. Zootopia''s top cop, a self-righteous rabbit, is hot on his tail, but when both become targets of a conspiracy, they''re forced to team up and discover even natural enemies can become best friends.',2016,'zootopia', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/CzvH6_e2a-U?rel=0&amp;showinfo=0'),
		(39, 'Ant-Man', 117, 'Armed with the astonishing ability to shrink in scale but increase in strength, con-man Scott Lang must embrace his inner-hero and help his mentor, Dr. Hank Pym, protect the secret behind his spectacular Ant-Man suit from a new generation of towering threats. Against seemingly insurmountable obstacles, Pym and Lang must plan and pull off a heist that will save the world.',2015,'ant_man', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/pWdKf3MneyI?rel=0&amp;showinfo=0'),
		(40, 'Iron Man', 126, 'After escaping from kidnappers using makeshift power armor, an ultra rich inventor and weapons maker turns his creation into a force for good by using it to fight crime. But his skills are stretched to the limit when he must face the evil Iron Monger.',2008,'iron_man', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/8hYlB38asDY?rel=0&amp;showinfo=0'),
		(41, 'Iron Man 2', 124, 'Now that his Super Hero secret has been revealed, Tony Stark''s life is more intense than ever. Everyone wants in on the Iron Man technology, whether for power or profit... But for Ivan Vanko, it''s revenge! Tony must once again suit up and face his most dangerous enemy yet, but not without a few new allies of his own.',2010,'iron_man_2', 40, 1.50, 'https://www.youtube-nocookie.com/embed/BoohRoVA9WQ?rel=0&amp;showinfo=0'),
		(42, 'Iron Man 3', 130, 'The brash-but-brilliant industrialist Tony Stark faces an enemy whose reach knows no bounds. When Stark finds his personal world destroyed at his enemy�s hands, he embarks on a harrowing quest to find those responsible.',2013,'iron_man_3', 41, 1.50, 'https://www.youtube-nocookie.com/embed/Ke1Y3P9D0Bc?rel=0&amp;showinfo=0'),
		(43, 'Guardians of the Galaxy', 120, 'Light years from Earth, 26 years after being abducted, Peter Quill finds himself the prime target of a manhunt after discovering an orb wanted by Ronan the Accuser.',2014,'guardians_of_the_galaxy', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/2XltzyLcu0g?rel=0&amp;showinfo=0'),
		(44, 'Harry Potter and the Half-Blood Prince', 153, 'The sixth year at Hogwarts School of Witchcraft and Wizardry kicks off with a bang for young Harry Potter when he inadvertently discovers a mysterious book that sheds light on the sordid life of the evil Lord Voldemort. Preparing for inevitable battle, Harry and Dumbledore turn to professor Horace Slughorn for help in pinpointing the weakness in Voldemort''s forces.',2009,'harry_potter_half_blood', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/jpCPvHJ6p90?rel=0&amp;showinfo=0')

INSERT INTO Movie_Directors
VALUES	(11, 1),(11, 2),
		(12, 1),(12, 2),
		(13,1),(13,2),
		(14,6),
		(15,6),
		(16,12),
		(17,16),
		(18,20),
		(19,12),
		(20,12),
		(21,19),
		(22,33),(22,34),
		(23,37),
		(24,41),
		(25,45),
		(26,12),
		(27,51),
		(28,55),
		(29,59),
		(30,62),
		(31,66),
		(32,68),
		(33, 70),
		(34, 73), (34, 74),
		(35, 74),
		(36, 78),(36, 79),
		(37, 80),
		(38, 82),(38, 83),
		(39, 87),
		(40, 91),
		(41, 91),
		(42, 96),
		(43, 97),
		(44, 101)

INSERT INTO Movie_Cast
VALUES  (11,3,'Neo'),(11,4,'Morpheus'),(11,5,'Trinity'),
		(12,3,'Neo'),(12,4,'Morpheus'),(12,5,'Trinity'),
		(13,3,'Neo'),(13,4,'Morpheus'),(13,5,'Trinity'),
		(14,7,'Don Vito Corleone'),(14,8,'Michael Corleone'),(14,9,'Sonny Corleone'),
		(15,8,'Michael Corleone'),(15,10,'Vito Corleone'),(15,11,'Tom Hagen'),
		(16,13,'Bruce Wayne'),(16,14,'Joker'),(16,15,'Harvey Dent'),
		(17,17,'Juror 8'),(17,18,'Juror 3'),(17,19,'Juror 1'),
		(18,21,'Vincent Vega'),(18,22,'Mia Wallace'),(18,23,'Jules Winnfield'),
		(19,13,'Bruce Wayne'),(19,24,'Bane'),(19,25,'Selina'),
		(20,26,'Cobb'),(20,27,'Arthur'),(20,28,'Ariadne'),
		(21,30,'Nina Sayers'),(21,31,'Lily'),(21,32,'Thomas Leroy'),
		(22,31,'Amy'),(22,35,'Carla'),(22,36,'Kiki'),
		(23,38,'Wade'),(23,39,'Vanessa'),(23,40,'Weasel'),
		(24,42,'Dr. Stephen Strange'),(24,43,'Mordo'),(24,44,'Christine Palmer'),
		(25,46,'Newt'),(25,47,'Tina'),(25,48,'Queenie'),
		(26,49,'Cooper'),(26,25,'Brand'),(26,50,'Murph'),
		(27,52,'Jason Bourne'),(27,53,'CIA Director Robert Dewey'),(27,54,'Heather Lee'),
		(28,56,'Abraham Lincoln'),(28,57,'Mary Todd Lincoln'),(28,58,'William Seward'),
		(29,26,'Teddy Daniels'),(29,60,'Rachel 1'),(29,61,'Chuck Aule'),
		(30,63,'Bilbo'),(30,64,'Gandalf'),(30,65,'Thorin'),
		(31,52,'Mark Watney'),(31,50,'Melissa Lewis'),(31,67,'Annie Montrose'),
		(32,26,'Hugh Glass'),(32,24,'John Fitzgerald'),(32,69,'Bridger'),
		(33, 71, 'Jake Morrison'),(33, 72, 'David Levison'),
		(34, 75, 'Po'),(34, 76, 'Li'),
		(35, 75, 'Po'),(35, 77, 'Tigress'),
		(36, 75, 'Po'),(36, 77, 'Tigress'),
		(37, 81, 'Paul Blart'),
		(38, 84, 'Judy Hopps'),(38, 85, 'Nick Wilde'),(38, 86, 'Chief Bogo'),
		(39, 88, 'Scott Lang'),(39, 89, 'Dr. Hank Pym'),(39, 90, 'Hope van Dyne'),
		(40, 92, 'Tony Stark'), (40, 93, 'Rhodey'),(40, 94, 'Pepper Potts'),
		(41, 92, 'Tony Stark'), (41, 93, 'Rhodey'),(41, 94, 'Pepper Potts'),
		(42, 92, 'Tony Stark'), (42, 95, 'Colonel James Rhodes'),(42, 94, 'Pepper Potts'),
		(43, 98, 'Peter Quill'), (43, 99, 'Groot'), (43, 100, 'Rocket'),
		(44, 102, 'Harry Potter'), (44, 103, 'Hermione Granger'), (44, 104, 'Ron Weasley')

INSERT INTO Genre
VALUES  ('Action'),
		('Adventure'),
		('Animation'),
		('Biography'),
		('Comedy'),
		('Crime'),
		('Documentary'),
		('Drama'),
		('Family'),
		('Fantasy'),
		('Film-Noir'),
		('History'),
		('Horror'),
		('Music'),
		('Musical'),
		('Mystery'),
		('Romance'),
		('Sci-Fi'),
		('Sport'),
		('Thriller'),
		('War'),
		('Western')

INSERT INTO Movie_Genre
VALUES	(11, 'Action'),(11, 'Sci-Fi'),
		(12, 'Action'),(12, 'Sci-Fi'),
		(13, 'Action'),(13, 'Sci-Fi'),
		(14, 'Crime'),(14, 'Drama'),
		(15, 'Crime'),(15, 'Drama'),
		(16, 'Action'),(16, 'Crime'),(16, 'Drama'),
		(17, 'Crime'),(17, 'Drama'),
		(18, 'Crime'),(18, 'Drama'),
		(19,'Action'),(19,'Thriller'),
		(20,'Action'),(20,'Adventure'),(20, 'Sci-Fi'),
		(21,'Drama'),(21,'Thriller'),
		(22,'Comedy'),
		(23,'Action'),(23,'Adventure'),(23, 'Comedy'),
		(24,'Action'),(24,'Adventure'),(24, 'Fantasy'),
		(25,'Adventure'),(25,'Family'),(25,'Fantasy'),
		(26,'Adventure'),(26,'Drama'),(26,'Sci-Fi'),
		(27,'Action'),(27,'Thriller'),
		(28,'Biography'),(28,'Drama'),(28,'History'),
		(29,'Mystery'),(29,'Thriller'),
		(30,'Adventure'),(30,'Fantasy'),
		(31,'Adventure'),(31,'Drama'),(31,'Sci-Fi'),
		(32,'Adventure'),(32,'Drama'),(32,'Thriller'),
		(33,'Adventure'),(33,'Action'),(33,'Sci-Fi'),
		(34,'Animation'),(34,'Action'),(34,'Adventure'),
		(35,'Animation'),(35,'Action'),(35,'Adventure'),
		(36,'Animation'),(36,'Action'),(36,'Adventure'),
		(37,'Action'),(37,'Comedy'),(37,'Crime'),
		(38,'Animation'),(38,'Adventure'),(38,'Comedy'),
		(39,'Action'),(39,'Adventure'),(39,'Comedy'),
		(40,'Action'),(40,'Adventure'),(40,'Sci-Fi'),
		(41,'Action'),(41,'Adventure'),(41,'Sci-Fi'),
		(42,'Action'),(42,'Adventure'),(42,'Sci-Fi'),
		(43,'Action'),(43,'Adventure'),(43,'Sci-Fi'),
		(44,'Adventure'),(44,'Family'),(44,'Fantasy')

INSERT INTO Country
VALUES ('Netherlands'),
	('Belgium'),
	('United Kingdom'),
	('Germany'), 
	('Sweden')

INSERT INTO Customer([Customer_Mail_Address],[Name],[Paypal_Account],[Subscription_Start],[Password],[Country_Name]) VALUES('ante.Maecenas.mi@pede.co.uk','Richard Meyer','arcu.vel@felispurus.com','02-Sep-2016','7680','Netherlands'),('Cras.eget@lacus.edu','Leslie York','purus.Duis@Donectempor.net','26-Jan-2016','7529','Netherlands'),('vestibulum.Mauris@lobortismauris.org','Ray Griffin','ligula@cursusNuncmauris.edu','02-Oct-2016','2704','Germany'),('arcu.Vestibulum.ante@parturientmontes.co.uk','Heidi Frank','nunc.nulla.vulputate@Integerurna.net','03-Feb-2016','9853','United Kingdom'),('id.libero.Donec@vitae.edu','Nyssa Buchanan','sapien.cursus.in@vulputatenisisem.org','21-Jun-2016','7677','United Kingdom'),('ornare@urnaVivamus.ca','Quinn Cantrell','Proin.mi.Aliquam@sem.org','19-Mar-2016','3315','Netherlands'),('lobortis.quis.pede@erategetipsum.edu','Colby Sims','cursus@natoquepenatibuset.ca','01-Nov-2016','5039','Germany'),('libero@gravidanunc.com','Hannah Pugh','In.condimentum@dictumaugue.net','24-Feb-2016','2588','Germany'),('ultrices@parturient.org','Jaquelyn Frank','fermentum.vel@enimsit.edu','10-Jan-2017','3177','Netherlands'),('Pellentesque.habitant.morbi@pellentesqueegetdictum.org','Ralph George','sagittis@nonloremvitae.co.uk','07-Nov-2016','2221','Germany'),('convallis.dolor@Pellentesquetincidunttempus.net','Kenyon Walter','vel@Suspendisseeleifend.co.uk','02-May-2016','9624','Germany'),('ultricies.ornare.elit@nisidictumaugue.co.uk','Nichole Franco','ut.nisi@semper.com','10-Dec-2016','3989','Netherlands'),('dolor@maurisut.net','Marah Rhodes','egestas.nunc.sed@ipsumprimisin.com','28-Apr-2016','7487','Netherlands'),('eget@et.ca','Calvin Evans','adipiscing.lobortis.risus@elit.edu','30-Aug-2016','1435','Belgium'),('Mauris@adipiscinglacusUt.co.uk','Ima Cobb','eu@tempusloremfringilla.co.uk','14-Jan-2016','5984','Netherlands'),('rutrum.lorem@Pellentesquehabitantmorbi.com','Julian Ware','Phasellus.in@euturpisNulla.com','09-Feb-2017','9943','United Kingdom'),('odio.Aliquam@convallisin.net','Iona Ramos','a@dolorFuscemi.org','16-Aug-2016','2230','Netherlands'),('Aliquam.ultrices.iaculis@Aliquam.org','Margaret Sanchez','Nunc.pulvinar.arcu@AliquamnislNulla.com','09-Oct-2016','3365','Sweden'),('vitae@aliquet.ca','Hadassah Finley','laoreet.lectus@NuncmaurisMorbi.edu','13-Sep-2016','2728','Sweden'),('pede@egetdictum.ca','Dacey Neal','pharetra@Etiamligula.co.uk','27-Jul-2016','6988','Sweden'),('magna@Donec.net','Ann Montgomery','massa@neceuismod.co.uk','11-Apr-2016','7671','United Kingdom'),('Nulla.aliquet.Proin@lacuspedesagittis.edu','Oleg Baker','porttitor.interdum.Sed@odioAliquamvulputate.co.uk','20-Sep-2016','9454','Sweden'),('ad.litora@afacilisis.ca','Haviva Higgins','dignissim.magna.a@nullaCras.edu','11-Oct-2016','2152','Germany'),('In@est.com','Hayley Higgins','felis@bibendumullamcorperDuis.edu','15-Mar-2017','2099','Netherlands'),('eu.dui@consequat.com','Halee Kidd','Cum.sociis.natoque@tristique.edu','20-Mar-2016','9791','Netherlands'),('nonummy.ut.molestie@quisturpis.edu','Zachery Phillips','Nullam.enim@nibh.co.uk','23-Aug-2016','2560','Germany'),('euismod.ac.fermentum@turpisvitae.edu','Amethyst Gentry','nisl.Maecenas@acfermentumvel.ca','06-Feb-2017','6712','United Kingdom'),('at@necurna.edu','Julie Delaney','suscipit.est.ac@penatibuset.net','30-Nov-2016','6411','Sweden'),('felis.adipiscing.fringilla@ametconsectetueradipiscing.ca','Katelyn Key','luctus.aliquet.odio@ideratEtiam.edu','01-Feb-2016','4286','United Kingdom'),('tristique@Aliquamultrices.com','Todd Garrison','Phasellus.nulla.Integer@duiFuscealiquam.ca','01-Mar-2017','4131','Sweden'),('elit@magnased.net','Cora Humphrey','orci@rutrum.org','08-Apr-2016','3445','Belgium'),('fames@accumsaninterdum.edu','Riley Davenport','lectus.convallis.est@Cumsociisnatoque.co.uk','10-Apr-2016','1718','Sweden'),('Aenean@suscipitestac.ca','Neil Bell','vel@antedictum.com','04-Feb-2017','7157','United Kingdom'),('eget@cursusIntegermollis.org','Dale Tyler','a.magna@neque.co.uk','16-Jun-2016','7965','Germany'),('scelerisque.neque.Nullam@dignissimmagnaa.org','Lillian Mcmahon','posuere.enim@Namconsequat.com','28-Aug-2016','5294','Belgium'),('purus@a.org','Benjamin Reed','feugiat.placerat.velit@nulla.co.uk','21-Aug-2016','1704','Belgium'),('odio@dictummiac.edu','Zenaida Cantrell','sem.mollis@malesuadavel.co.uk','05-May-2016','3318','Netherlands'),('nec.tempus@egestasFusce.org','Maggie Sutton','imperdiet.dictum.magna@egetmollis.co.uk','12-Dec-2016','6457','United Kingdom'),('sollicitudin.orci@utlacusNulla.com','Owen David','justo.nec.ante@arcuVestibulum.edu','31-Oct-2016','9846','Belgium'),('ante.ipsum@tellusPhaselluselit.edu','Medge Galloway','semper.egestas.urna@a.edu','18-Mar-2017','9106','Netherlands'),('sed.pede@sed.com','Phillip Wheeler','dolor.Nulla.semper@sed.net','27-Mar-2016','3792','Germany'),('eget.ipsum.Donec@risusNuncac.com','Melissa Allison','hendrerit.Donec.porttitor@Quisqueornaretortor.ca','05-Jun-2016','6452','Netherlands'),('mattis.ornare@ornaretortor.com','Deacon Le','tellus@Duis.ca','14-Feb-2017','2426','Belgium'),('volutpat@Nunc.co.uk','Cyrus Winters','ac.feugiat.non@blanditcongueIn.org','01-May-2016','5873','Netherlands'),('pharetra@fringillaDonec.co.uk','Wesley Espinoza','sapien@morbi.co.uk','21-Feb-2017','4306','Sweden'),('at.sem@risusvarius.com','Ifeoma Koch','gravida.Aliquam@sit.com','02-May-2016','3798','Netherlands'),('porta.elit.a@Curabitur.net','Katelyn Bender','mattis.Integer.eu@malesuadavel.net','29-Feb-2016','2058','Sweden'),('nibh@accumsan.ca','Harper Fry','In@ultricesmaurisipsum.co.uk','13-Mar-2016','6578','Netherlands'),('nibh.Aliquam.ornare@tincidunt.com','Lilah Moses','at.pede@perinceptoshymenaeos.org','08-Mar-2017','5361','United Kingdom'),('lorem@Pellentesquetincidunt.ca','Hadley Rivers','et@idmagnaet.ca','26-Oct-2016','5678','United Kingdom'),('a.nunc@fringilla.com','Maia Mills','mollis.vitae@atnisi.com','06-Jan-2016','4602','United Kingdom'),('Phasellus.in.felis@egestas.org','Maisie Atkinson','justo@Donec.ca','15-Jul-2016','2658','Sweden'),('neque.vitae.semper@arcu.ca','Lewis Sharpe','In@In.net','21-Dec-2016','1645','United Kingdom'),('luctus@Vivamusnon.org','Kane Baxter','in@gravidanonsollicitudin.com','30-Jul-2016','4408','Sweden'),('ut.sem.Nulla@facilisismagnatellus.org','John Vega','nonummy.ut@Nullamlobortis.com','27-Jul-2016','7542','Netherlands'),('auctor.non@Seddictum.net','Kasimir Perez','ac@convallisincursus.ca','12-Aug-2016','4960','Germany'),('dolor.Donec@rutrumnon.org','Bell Spence','in.tempus.eu@et.com','09-Feb-2017','3077','Sweden'),('quam.Pellentesque@vestibulumlorem.ca','Caldwell Gates','cubilia.Curae.Phasellus@enim.net','26-Jun-2016','4120','Netherlands'),('consequat.nec.mollis@duisemper.edu','Scott Becker','non@nec.edu','28-Feb-2016','5178','United Kingdom'),('in.tempus@aliquetsem.edu','Angela Bird','luctus.felis@velitPellentesqueultricies.com','31-May-2016','5842','Germany'),('aliquet.libero.Integer@mifelisadipiscing.co.uk','Tatum Hawkins','luctus@nibhAliquamornare.net','15-Jun-2016','6463','Germany'),('sociis.natoque.penatibus@purusDuiselementum.edu','Armand Oneal','sit@commodo.ca','12-Feb-2017','2683','United Kingdom'),('Duis.gravida@Inlorem.org','Ahmed Morton','ullamcorper.magna@Etiamlaoreet.com','21-Aug-2016','9980','Sweden'),('nisl.sem.consequat@fringillapurusmauris.net','Kirsten Wood','parturient.montes@Nuncmauris.co.uk','02-Jan-2016','3999','Netherlands'),('Ut.nec@sagittis.net','Cade Odonnell','ac.mattis@lobortisnisinibh.org','24-Feb-2016','7236','Netherlands'),('Vivamus@semut.co.uk','Price Rojas','Nulla.eget.metus@Aeneansed.net','03-Mar-2017','6603','Germany'),('congue.In.scelerisque@Vivamus.co.uk','Kerry Fletcher','Vestibulum.ante.ipsum@Morbiaccumsan.org','18-Feb-2016','6388','Sweden'),('mauris.sagittis.placerat@auguescelerisquemollis.com','Moses Wynn','ultrices.mauris@AliquamnislNulla.com','02-Jul-2016','7801','Sweden'),('metus.Aenean@nibhPhasellus.edu','Brianna Acevedo','nec@NuncmaurisMorbi.org','11-Mar-2016','7842','Belgium'),('malesuada@posuerevulputate.org','Morgan Duke','interdum.libero.dui@fermentum.co.uk','21-Sep-2016','1802','Sweden'),('ligula.consectetuer.rhoncus@orci.edu','Joseph Johns','lorem.ut@Fuscemilorem.ca','23-Aug-2016','9732','Netherlands'),('Vivamus.nibh@nonenimcommodo.ca','Rashad Booker','ante@turpisvitaepurus.co.uk','19-Mar-2017','3551','Germany'),('Sed.molestie@pretiumaliquet.ca','Abraham Jennings','et.magnis.dis@arcuMorbi.co.uk','24-Jan-2017','3129','Netherlands'),('quis.arcu@Etiam.com','Channing Vaughan','et.libero@egestas.co.uk','02-Jul-2016','8566','Germany'),('ipsum.primis.in@ultrices.com','Kirsten Vaughn','amet.consectetuer.adipiscing@iaculislacus.org','02-Jan-2016','5176','Belgium'),('Aliquam.erat.volutpat@felis.org','Neil Howard','diam@eratVivamus.edu','07-Jul-2016','3538','Germany'),('orci.luctus.et@felisorci.com','Hector Hayden','ut@vitaerisus.edu','26-Jun-2016','5451','Netherlands'),('fringilla.ornare@vitaemauris.ca','Jacqueline Stanton','velit.dui@eueuismodac.org','21-Jul-2016','2645','Netherlands'),('mauris.eu.elit@sitamet.ca','Karyn Lindsay','purus@massaSuspendisseeleifend.net','13-Nov-2016','9383','Germany'),('id.risus@ipsumSuspendissenon.com','Raven Hansen','ullamcorper.viverra@utdolordapibus.ca','16-Feb-2016','2153','United Kingdom'),('orci.consectetuer.euismod@justosit.co.uk','Kyle Park','sollicitudin.adipiscing.ligula@euaccumsansed.net','20-Feb-2017','6018','United Kingdom'),('neque.sed.sem@CrasinterdumNunc.org','Ariana Robles','erat.Vivamus.nisi@ipsum.co.uk','19-Jan-2016','5823','Germany'),('lectus.pede.ultrices@ametultriciessem.org','Blythe Mccray','feugiat.nec.diam@vel.org','08-Mar-2017','5773','United Kingdom'),('Nulla.facilisis@nec.net','Quemby Moss','libero.Integer.in@orciquis.net','03-Jun-2016','7002','Belgium'),('semper.auctor.Mauris@auctorMaurisvel.ca','Barrett Levine','ut.pharetra.sed@etmagnisdis.ca','01-Apr-2016','8043','United Kingdom'),('molestie@portaelit.co.uk','Dean Davis','lectus.ante.dictum@Nullam.co.uk','13-Nov-2016','3329','Netherlands'),('ridiculus@leo.edu','Abdul Hall','elit.fermentum@dolorelitpellentesque.com','04-Oct-2016','9092','Sweden'),('tempor@diamat.co.uk','Adara Gibbs','dui.Fusce.aliquam@nunc.ca','31-Jan-2017','3513','Belgium'),('Mauris.vestibulum@erosnon.net','Zachery Gates','eu.nibh@aliquetmetusurna.org','31-Jan-2017','9371','Belgium'),('tincidunt.neque.vitae@Nullam.edu','Jackson Richardson','nec.ante@duiFusce.org','13-Sep-2016','6267','United Kingdom'),('Proin.non@Nullam.com','Preston Durham','Mauris.vel@scelerisque.ca','11-Jun-2016','4458','Germany'),('Nunc@auctorvitaealiquet.net','Derek Morton','dui.Fusce.diam@uterosnon.net','06-May-2016','2319','Netherlands'),('mi@est.com','Brendan Schultz','nunc@euismodet.co.uk','25-Aug-2016','6513','Netherlands'),('feugiat@rhoncus.edu','Kyra Fernandez','metus.Aenean@diamPellentesque.co.uk','05-Oct-2016','1312','Germany'),('mi.tempor@mollis.com','Hamish Lopez','nunc.ac@egetipsumDonec.org','27-Feb-2017','1248','Sweden'),('risus.Duis@pedenec.edu','Breanna Johns','felis.eget.varius@massa.com','14-Jul-2016','7187','Germany'),('Quisque.tincidunt@elit.edu','William Melendez','lorem.fringilla@velit.com','17-Nov-2016','9241','Germany'),('hendrerit@duiSuspendisseac.edu','Leandra Christensen','Aliquam.vulputate.ullamcorper@rutrumloremac.com','23-Jul-2016','3120','United Kingdom'),('malesuada.augue@Duissitamet.ca','Amena Avery','est.Nunc.ullamcorper@Proin.ca','29-Jul-2016','8815','United Kingdom'),('Nulla.facilisis.Suspendisse@etultricesposuere.ca','Odette Huber','hendrerit.consectetuer.cursus@nibhAliquamornare.edu','21-Jun-2016','7676','United Kingdom')

INSERT INTO Watchhistory([Movie_id],[Customer_Mail_Address],[Watch_Date],[Price],[Invoiced]) 
VALUES('11','ante.Maecenas.mi@pede.co.uk','10-Aug-2016','0','0'),
('32','ante.Maecenas.mi@pede.co.uk','11-Oct-2015','0','0'),
('32','eget@et.ca','12-Nov-2015','0','0'),
('33','eget@et.ca','22-Aug-2016','0','0'),
('32','vestibulum.Mauris@lobortismauris.org','15-Jul-2016','0','0'),
('33','vestibulum.Mauris@lobortismauris.org','02-Nov-2016','0','0'),
('34','vestibulum.Mauris@lobortismauris.org','15-Oct-2016','0','0'),
('35','id.libero.Donec@vitae.edu','27-Nov-2015','0','0'),
('36','id.libero.Donec@vitae.edu','10-Jul-2015','0','0'),
('38','id.libero.Donec@vitae.edu','27-Sep-2015','0','0'),
('40','id.libero.Donec@vitae.edu','25-Aug-2016','0','0'),
('11','lobortis.quis.pede@erategetipsum.edu','03-Jul-2016','0','0'),
('20','lobortis.quis.pede@erategetipsum.edu','30-Sep-2016','0','0'),
('23','libero@gravidanunc.com','10-Mar-2016','0','0'),
('14','libero@gravidanunc.com','07-Jul-2016','0','0'),
('37','libero@gravidanunc.com','11-Sep-2015','0','0'),
('12','Pellentesque.habitant.morbi@pellentesqueegetdictum.org','22-Nov-2015','0','0'),
('16','Pellentesque.habitant.morbi@pellentesqueegetdictum.org','01-Sep-2016','0','0'),
('19','convallis.dolor@Pellentesquetincidunttempus.net','09-May-2016','0','0'),
('44','convallis.dolor@Pellentesquetincidunttempus.net','10-Apr-2015','0','0'),
('37','convallis.dolor@Pellentesquetincidunttempus.net','27-Jan-2016','0','0'),
('25','ultricies.ornare.elit@nisidictumaugue.co.uk','10-Apr-2015','0','0'),
('13','ultricies.ornare.elit@nisidictumaugue.co.uk','30-Sep-2015','0','0'),
('23','dolor@maurisut.net','27-Aug-2015','0','0'),
('33','dolor@maurisut.net','23-Apr-2016','0','0'),
('41','eget@et.ca','11-Sep-2015','0','0'),
('12','eget@et.ca','04-Oct-2015','0','0'),
('27','Mauris@adipiscinglacusUt.co.uk','16-Apr-2015','0','0'),
('26','Mauris@adipiscinglacusUt.co.uk','16-Dec-2015','0','0'),
('13','ultrices@parturient.org','05-May-2016','0','0'),
('18','ultrices@parturient.org','14-Sep-2015','0','0'),
('39','odio.Aliquam@convallisin.net','02-May-2016','0','0'),
('26','odio.Aliquam@convallisin.net','28-Jul-2015','0','0'),
('25','Aliquam.ultrices.iaculis@Aliquam.org','03-Jun-2015','0','0'),
('11','Aliquam.ultrices.iaculis@Aliquam.org','16-Sep-2016','0','0'),
('12','Aliquam.ultrices.iaculis@Aliquam.org','19-Jun-2016','0','0'),
('34','pede@egetdictum.ca','17-Nov-2015','0','0'),
('35','pede@egetdictum.ca','22-Jun-2016','0','0'),
('36','pede@egetdictum.ca','08-Jul-2016','0','0'),
('37','Nulla.aliquet.Proin@lacuspedesagittis.edu','16-Sep-2015','0','0'),
('38','Nulla.aliquet.Proin@lacuspedesagittis.edu','05-May-2016','0','0'),
('29','In@est.com','17-Sep-2015','0','0'),
('39','In@est.com','22-Aug-2015','0','0'),
('40','In@est.com','06-Aug-2015','0','0'),
('41','In@est.com','31-Jan-2016','0','0'),
('42','eu.dui@consequat.com','16-Jul-2015','0','0'),
('43','eu.dui@consequat.com','18-May-2015','0','0'),
('44','eu.dui@consequat.com','27-Apr-2016','0','0'),
('11','euismod.ac.fermentum@turpisvitae.edu','18-Sep-2016','0','0'),
('12','euismod.ac.fermentum@turpisvitae.edu','29-Oct-2016','0','0'),
('13','euismod.ac.fermentum@turpisvitae.edu','08-Mar-2016','0','0'),
('14','felis.adipiscing.fringilla@ametconsectetueradipiscing.ca','02-Dec-2016','0','0'),
('15','felis.adipiscing.fringilla@ametconsectetueradipiscing.ca','03-Dec-2015','0','0'),
('16','tristique@Aliquamultrices.com','18-May-2016','0','0'),
('17','tristique@Aliquamultrices.com','19-May-2016','0','0'),
('18','tristique@Aliquamultrices.com','24-Jan-2016','0','0'),
('19','tristique@Aliquamultrices.com','07-Sep-2016','0','0'),
('20','fames@accumsaninterdum.edu','17-Jan-2016','0','0'),
('21','fames@accumsaninterdum.edu','06-Jan-2016','0','0'),
('22','Aenean@suscipitestac.ca','08-Aug-2015','0','0'),
('23','Aenean@suscipitestac.ca','02-Oct-2016','0','0'),
('24','Aenean@suscipitestac.ca','20-Mar-2016','0','0'),
('25','scelerisque.neque.Nullam@dignissimmagnaa.org','02-Nov-2015','0','0'),
('26','scelerisque.neque.Nullam@dignissimmagnaa.org','27-May-2015','0','0'),
('27','purus@a.org','08-Feb-2016','0','0'),
('28','purus@a.org','12-May-2016','0','0'),
('29','odio@dictummiac.edu','24-Jan-2016','0','0'),
('30','odio@dictummiac.edu','19-Jun-2016','0','0'),
('31','odio@dictummiac.edu','02-Mar-2016','0','0'),
('32','odio@dictummiac.edu','26-Jul-2016','0','0'),
('33','sollicitudin.orci@utlacusNulla.com','27-Oct-2015','0','0'),
('34','sollicitudin.orci@utlacusNulla.com','09-Oct-2016','0','0'),
('35','ante.ipsum@tellusPhaselluselit.edu','02-May-2016','0','0'),
('36','ante.ipsum@tellusPhaselluselit.edu','27-Jul-2016','0','0'),
('37','sed.pede@sed.com','05-Dec-2016','0','0'),
('38','sed.pede@sed.com','11-Aug-2016','0','0'),
('39','sed.pede@sed.com','24-Dec-2015','0','0'),
('40','eget.ipsum.Donec@risusNuncac.com','25-Jan-2016','0','0'),
('41','eget.ipsum.Donec@risusNuncac.com','21-Apr-2015','0','0'),
('42','mattis.ornare@ornaretortor.com','07-May-2015','0','0'),
('43','mattis.ornare@ornaretortor.com','30-Aug-2016','0','0'),
('44','volutpat@Nunc.co.uk','16-Feb-2016','0','0'),
('20','pharetra@fringillaDonec.co.uk','29-May-2016','0','0'),
('11','pharetra@fringillaDonec.co.uk','27-Jun-2016','0','0'),
('12','pharetra@fringillaDonec.co.uk','05-Sep-2015','0','0'),
('31','at.sem@risusvarius.com','15-Aug-2016','0','0'),
('32','at.sem@risusvarius.com','11-Jul-2015','0','0'),
('17','at.sem@risusvarius.com','18-Jul-2016','0','0'),
('18','nibh@accumsan.ca','20-May-2016','0','0'),
('27','nibh@accumsan.ca','12-Nov-2016','0','0'),
('40','nibh.Aliquam.ornare@tincidunt.com','07-Jul-2016','0','0'),
('32','nibh.Aliquam.ornare@tincidunt.com','02-Jun-2016','0','0'),
('33','lorem@Pellentesquetincidunt.ca','20-Oct-2016','0','0'),
('43','lorem@Pellentesquetincidunt.ca','03-Aug-2016','0','0'),
('18','a.nunc@fringilla.com','23-Jul-2016','0','0'),
('11','Phasellus.in.felis@egestas.org','25-May-2016','0','0'),
('23','neque.vitae.semper@arcu.ca','27-Jan-2016','0','0'),
('15','luctus@Vivamusnon.org','03-Jun-2015','0','0'),
('38','luctus@Vivamusnon.org','09-Feb-2016','0','0'),
('19','ut.sem.Nulla@facilisismagnatellus.org','03-Jul-2015','0','0')

/* QUERIES DIE WE GAAN GEBRUIKEN OP DE WEBSITE */
/* Naam van Movie Director */
SELECT P.Firstname, P.Lastname
FROM Person P, Movie M, Movie_Directors MD
WHERE M.Movie_Id = 24
AND M.Movie_id = MD.Movie_id AND MD.Person_id = P.Person_id

/* Laatst toegevoegde films */
SELECT TOP 10 M.Movie_id, M.Title, M.Cover_Image FROM Movie M ORDER BY Publication_Year DESC

/* Top films */
SELECT TOP 6 M.Movie_id, M.Title, M.Cover_Image FROM Movie M WHERE M.Movie_id IN (SELECT movie_id FROM WatchHistory)
ORDER BY (COUNT(movie_id) OVER(PARTITION BY movie_id)) DESC

/* WatchHistory */
select * from WatchHistory

/* Recent bekeken */
SELECT TOP 5 M.Movie_id, M.Title, M.Cover_Image FROM Movie M WHERE movie_id IN (SELECT WH.movie_id FROM WatchHistory WH WHERE WH.customer_mail_address = 'jari_barca@gmail.com')