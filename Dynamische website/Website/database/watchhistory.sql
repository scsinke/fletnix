-- FLETNIX 100 random customers
USE FLETNIX
GO

--Insert countries
INSERT INTO Country(country_name)
VALUES('Netherlands'),
	('Belgium'),
	('United Kingdom'),
	('Germany'), 
	('Sweden')


-- CUSTOMER SUB. START:
/* 
Tussen:
	01-01-2016 (1-januari-2016)
EN:
	25-03-2016 (25-maart-2016)
*/
--CUSTOMER WATCH DATE:
/*
Tussen:
	01-04-2016 (1-april-2016)
EN:
	09-12-2016 (9-december-2016)
*/

-- INSERT IN CUSTOMER
INSERT INTO Customer([customer_mail_address],[name],[paypal_account],[subscription_start],[password],[country_name]) VALUES('ante.Maecenas.mi@pede.co.uk','Richard Meyer','arcu.vel@felispurus.com','02-Sep-2016','7680','Netherlands'),('Cras.eget@lacus.edu','Leslie York','purus.Duis@Donectempor.net','26-Jan-2016','7529','Netherlands'),('vestibulum.Mauris@lobortismauris.org','Ray Griffin','ligula@cursusNuncmauris.edu','02-Oct-2016','2704','Germany'),('arcu.Vestibulum.ante@parturientmontes.co.uk','Heidi Frank','nunc.nulla.vulputate@Integerurna.net','03-Feb-2016','9853','United Kingdom'),('id.libero.Donec@vitae.edu','Nyssa Buchanan','sapien.cursus.in@vulputatenisisem.org','21-Jun-2016','7677','United Kingdom'),('ornare@urnaVivamus.ca','Quinn Cantrell','Proin.mi.Aliquam@sem.org','19-Mar-2016','3315','Netherlands'),('lobortis.quis.pede@erategetipsum.edu','Colby Sims','cursus@natoquepenatibuset.ca','01-Nov-2016','5039','Germany'),('libero@gravidanunc.com','Hannah Pugh','In.condimentum@dictumaugue.net','24-Feb-2016','2588','Germany'),('ultrices@parturient.org','Jaquelyn Frank','fermentum.vel@enimsit.edu','10-Jan-2017','3177','Netherlands'),('Pellentesque.habitant.morbi@pellentesqueegetdictum.org','Ralph George','sagittis@nonloremvitae.co.uk','07-Nov-2016','2221','Germany'),('convallis.dolor@Pellentesquetincidunttempus.net','Kenyon Walter','vel@Suspendisseeleifend.co.uk','02-May-2016','9624','Germany'),('ultricies.ornare.elit@nisidictumaugue.co.uk','Nichole Franco','ut.nisi@semper.com','10-Dec-2016','3989','Netherlands'),('dolor@maurisut.net','Marah Rhodes','egestas.nunc.sed@ipsumprimisin.com','28-Apr-2016','7487','Netherlands'),('eget@et.ca','Calvin Evans','adipiscing.lobortis.risus@elit.edu','30-Aug-2016','1435','Belgium'),('Mauris@adipiscinglacusUt.co.uk','Ima Cobb','eu@tempusloremfringilla.co.uk','14-Jan-2016','5984','Netherlands'),('rutrum.lorem@Pellentesquehabitantmorbi.com','Julian Ware','Phasellus.in@euturpisNulla.com','09-Feb-2017','9943','United Kingdom'),('odio.Aliquam@convallisin.net','Iona Ramos','a@dolorFuscemi.org','16-Aug-2016','2230','Netherlands'),('Aliquam.ultrices.iaculis@Aliquam.org','Margaret Sanchez','Nunc.pulvinar.arcu@AliquamnislNulla.com','09-Oct-2016','3365','Sweden'),('vitae@aliquet.ca','Hadassah Finley','laoreet.lectus@NuncmaurisMorbi.edu','13-Sep-2016','2728','Sweden'),('pede@egetdictum.ca','Dacey Neal','pharetra@Etiamligula.co.uk','27-Jul-2016','6988','Sweden'),('magna@Donec.net','Ann Montgomery','massa@neceuismod.co.uk','11-Apr-2016','7671','United Kingdom'),('Nulla.aliquet.Proin@lacuspedesagittis.edu','Oleg Baker','porttitor.interdum.Sed@odioAliquamvulputate.co.uk','20-Sep-2016','9454','Sweden'),('ad.litora@afacilisis.ca','Haviva Higgins','dignissim.magna.a@nullaCras.edu','11-Oct-2016','2152','Germany'),('In@est.com','Hayley Higgins','felis@bibendumullamcorperDuis.edu','15-Mar-2017','2099','Netherlands'),('eu.dui@consequat.com','Halee Kidd','Cum.sociis.natoque@tristique.edu','20-Mar-2016','9791','Netherlands'),('nonummy.ut.molestie@quisturpis.edu','Zachery Phillips','Nullam.enim@nibh.co.uk','23-Aug-2016','2560','Germany'),('euismod.ac.fermentum@turpisvitae.edu','Amethyst Gentry','nisl.Maecenas@acfermentumvel.ca','06-Feb-2017','6712','United Kingdom'),('at@necurna.edu','Julie Delaney','suscipit.est.ac@penatibuset.net','30-Nov-2016','6411','Sweden'),('felis.adipiscing.fringilla@ametconsectetueradipiscing.ca','Katelyn Key','luctus.aliquet.odio@ideratEtiam.edu','01-Feb-2016','4286','United Kingdom'),('tristique@Aliquamultrices.com','Todd Garrison','Phasellus.nulla.Integer@duiFuscealiquam.ca','01-Mar-2017','4131','Sweden'),('elit@magnased.net','Cora Humphrey','orci@rutrum.org','08-Apr-2016','3445','Belgium'),('fames@accumsaninterdum.edu','Riley Davenport','lectus.convallis.est@Cumsociisnatoque.co.uk','10-Apr-2016','1718','Sweden'),('Aenean@suscipitestac.ca','Neil Bell','vel@antedictum.com','04-Feb-2017','7157','United Kingdom'),('eget@cursusIntegermollis.org','Dale Tyler','a.magna@neque.co.uk','16-Jun-2016','7965','Germany'),('scelerisque.neque.Nullam@dignissimmagnaa.org','Lillian Mcmahon','posuere.enim@Namconsequat.com','28-Aug-2016','5294','Belgium'),('purus@a.org','Benjamin Reed','feugiat.placerat.velit@nulla.co.uk','21-Aug-2016','1704','Belgium'),('odio@dictummiac.edu','Zenaida Cantrell','sem.mollis@malesuadavel.co.uk','05-May-2016','3318','Netherlands'),('nec.tempus@egestasFusce.org','Maggie Sutton','imperdiet.dictum.magna@egetmollis.co.uk','12-Dec-2016','6457','United Kingdom'),('sollicitudin.orci@utlacusNulla.com','Owen David','justo.nec.ante@arcuVestibulum.edu','31-Oct-2016','9846','Belgium'),('ante.ipsum@tellusPhaselluselit.edu','Medge Galloway','semper.egestas.urna@a.edu','18-Mar-2017','9106','Netherlands'),('sed.pede@sed.com','Phillip Wheeler','dolor.Nulla.semper@sed.net','27-Mar-2016','3792','Germany'),('eget.ipsum.Donec@risusNuncac.com','Melissa Allison','hendrerit.Donec.porttitor@Quisqueornaretortor.ca','05-Jun-2016','6452','Netherlands'),('mattis.ornare@ornaretortor.com','Deacon Le','tellus@Duis.ca','14-Feb-2017','2426','Belgium'),('volutpat@Nunc.co.uk','Cyrus Winters','ac.feugiat.non@blanditcongueIn.org','01-May-2016','5873','Netherlands'),('pharetra@fringillaDonec.co.uk','Wesley Espinoza','sapien@morbi.co.uk','21-Feb-2017','4306','Sweden'),('at.sem@risusvarius.com','Ifeoma Koch','gravida.Aliquam@sit.com','02-May-2016','3798','Netherlands'),('porta.elit.a@Curabitur.net','Katelyn Bender','mattis.Integer.eu@malesuadavel.net','29-Feb-2016','2058','Sweden'),('nibh@accumsan.ca','Harper Fry','In@ultricesmaurisipsum.co.uk','13-Mar-2016','6578','Netherlands'),('nibh.Aliquam.ornare@tincidunt.com','Lilah Moses','at.pede@perinceptoshymenaeos.org','08-Mar-2017','5361','United Kingdom'),('lorem@Pellentesquetincidunt.ca','Hadley Rivers','et@idmagnaet.ca','26-Oct-2016','5678','United Kingdom'),('a.nunc@fringilla.com','Maia Mills','mollis.vitae@atnisi.com','06-Jan-2016','4602','United Kingdom'),('Phasellus.in.felis@egestas.org','Maisie Atkinson','justo@Donec.ca','15-Jul-2016','2658','Sweden'),('neque.vitae.semper@arcu.ca','Lewis Sharpe','In@In.net','21-Dec-2016','1645','United Kingdom'),('luctus@Vivamusnon.org','Kane Baxter','in@gravidanonsollicitudin.com','30-Jul-2016','4408','Sweden'),('ut.sem.Nulla@facilisismagnatellus.org','John Vega','nonummy.ut@Nullamlobortis.com','27-Jul-2016','7542','Netherlands'),('auctor.non@Seddictum.net','Kasimir Perez','ac@convallisincursus.ca','12-Aug-2016','4960','Germany'),('dolor.Donec@rutrumnon.org','Bell Spence','in.tempus.eu@et.com','09-Feb-2017','3077','Sweden'),('quam.Pellentesque@vestibulumlorem.ca','Caldwell Gates','cubilia.Curae.Phasellus@enim.net','26-Jun-2016','4120','Netherlands'),('consequat.nec.mollis@duisemper.edu','Scott Becker','non@nec.edu','28-Feb-2016','5178','United Kingdom'),('in.tempus@aliquetsem.edu','Angela Bird','luctus.felis@velitPellentesqueultricies.com','31-May-2016','5842','Germany'),('aliquet.libero.Integer@mifelisadipiscing.co.uk','Tatum Hawkins','luctus@nibhAliquamornare.net','15-Jun-2016','6463','Germany'),('sociis.natoque.penatibus@purusDuiselementum.edu','Armand Oneal','sit@commodo.ca','12-Feb-2017','2683','United Kingdom'),('Duis.gravida@Inlorem.org','Ahmed Morton','ullamcorper.magna@Etiamlaoreet.com','21-Aug-2016','9980','Sweden'),('nisl.sem.consequat@fringillapurusmauris.net','Kirsten Wood','parturient.montes@Nuncmauris.co.uk','02-Jan-2016','3999','Netherlands'),('Ut.nec@sagittis.net','Cade Odonnell','ac.mattis@lobortisnisinibh.org','24-Feb-2016','7236','Netherlands'),('Vivamus@semut.co.uk','Price Rojas','Nulla.eget.metus@Aeneansed.net','03-Mar-2017','6603','Germany'),('congue.In.scelerisque@Vivamus.co.uk','Kerry Fletcher','Vestibulum.ante.ipsum@Morbiaccumsan.org','18-Feb-2016','6388','Sweden'),('mauris.sagittis.placerat@auguescelerisquemollis.com','Moses Wynn','ultrices.mauris@AliquamnislNulla.com','02-Jul-2016','7801','Sweden'),('metus.Aenean@nibhPhasellus.edu','Brianna Acevedo','nec@NuncmaurisMorbi.org','11-Mar-2016','7842','Belgium'),('malesuada@posuerevulputate.org','Morgan Duke','interdum.libero.dui@fermentum.co.uk','21-Sep-2016','1802','Sweden'),('ligula.consectetuer.rhoncus@orci.edu','Joseph Johns','lorem.ut@Fuscemilorem.ca','23-Aug-2016','9732','Netherlands'),('Vivamus.nibh@nonenimcommodo.ca','Rashad Booker','ante@turpisvitaepurus.co.uk','19-Mar-2017','3551','Germany'),('Sed.molestie@pretiumaliquet.ca','Abraham Jennings','et.magnis.dis@arcuMorbi.co.uk','24-Jan-2017','3129','Netherlands'),('quis.arcu@Etiam.com','Channing Vaughan','et.libero@egestas.co.uk','02-Jul-2016','8566','Germany'),('ipsum.primis.in@ultrices.com','Kirsten Vaughn','amet.consectetuer.adipiscing@iaculislacus.org','02-Jan-2016','5176','Belgium'),('Aliquam.erat.volutpat@felis.org','Neil Howard','diam@eratVivamus.edu','07-Jul-2016','3538','Germany'),('orci.luctus.et@felisorci.com','Hector Hayden','ut@vitaerisus.edu','26-Jun-2016','5451','Netherlands'),('fringilla.ornare@vitaemauris.ca','Jacqueline Stanton','velit.dui@eueuismodac.org','21-Jul-2016','2645','Netherlands'),('mauris.eu.elit@sitamet.ca','Karyn Lindsay','purus@massaSuspendisseeleifend.net','13-Nov-2016','9383','Germany'),('id.risus@ipsumSuspendissenon.com','Raven Hansen','ullamcorper.viverra@utdolordapibus.ca','16-Feb-2016','2153','United Kingdom'),('orci.consectetuer.euismod@justosit.co.uk','Kyle Park','sollicitudin.adipiscing.ligula@euaccumsansed.net','20-Feb-2017','6018','United Kingdom'),('neque.sed.sem@CrasinterdumNunc.org','Ariana Robles','erat.Vivamus.nisi@ipsum.co.uk','19-Jan-2016','5823','Germany'),('lectus.pede.ultrices@ametultriciessem.org','Blythe Mccray','feugiat.nec.diam@vel.org','08-Mar-2017','5773','United Kingdom'),('Nulla.facilisis@nec.net','Quemby Moss','libero.Integer.in@orciquis.net','03-Jun-2016','7002','Belgium'),('semper.auctor.Mauris@auctorMaurisvel.ca','Barrett Levine','ut.pharetra.sed@etmagnisdis.ca','01-Apr-2016','8043','United Kingdom'),('molestie@portaelit.co.uk','Dean Davis','lectus.ante.dictum@Nullam.co.uk','13-Nov-2016','3329','Netherlands'),('ridiculus@leo.edu','Abdul Hall','elit.fermentum@dolorelitpellentesque.com','04-Oct-2016','9092','Sweden'),('tempor@diamat.co.uk','Adara Gibbs','dui.Fusce.aliquam@nunc.ca','31-Jan-2017','3513','Belgium'),('Mauris.vestibulum@erosnon.net','Zachery Gates','eu.nibh@aliquetmetusurna.org','31-Jan-2017','9371','Belgium'),('tincidunt.neque.vitae@Nullam.edu','Jackson Richardson','nec.ante@duiFusce.org','13-Sep-2016','6267','United Kingdom'),('Proin.non@Nullam.com','Preston Durham','Mauris.vel@scelerisque.ca','11-Jun-2016','4458','Germany'),('Nunc@auctorvitaealiquet.net','Derek Morton','dui.Fusce.diam@uterosnon.net','06-May-2016','2319','Netherlands'),('mi@est.com','Brendan Schultz','nunc@euismodet.co.uk','25-Aug-2016','6513','Netherlands'),('feugiat@rhoncus.edu','Kyra Fernandez','metus.Aenean@diamPellentesque.co.uk','05-Oct-2016','1312','Germany'),('mi.tempor@mollis.com','Hamish Lopez','nunc.ac@egetipsumDonec.org','27-Feb-2017','1248','Sweden'),('risus.Duis@pedenec.edu','Breanna Johns','felis.eget.varius@massa.com','14-Jul-2016','7187','Germany'),('Quisque.tincidunt@elit.edu','William Melendez','lorem.fringilla@velit.com','17-Nov-2016','9241','Germany'),('hendrerit@duiSuspendisseac.edu','Leandra Christensen','Aliquam.vulputate.ullamcorper@rutrumloremac.com','23-Jul-2016','3120','United Kingdom'),('malesuada.augue@Duissitamet.ca','Amena Avery','est.Nunc.ullamcorper@Proin.ca','29-Jul-2016','8815','United Kingdom'),('Nulla.facilisis.Suspendisse@etultricesposuere.ca','Odette Huber','hendrerit.consectetuer.cursus@nibhAliquamornare.edu','21-Jun-2016','7676','United Kingdom');

--INSERT IN WATCHHISTORY
INSERT INTO Watchhistory([movie_id],[customer_mail_address],[watch_date],[price],[invoiced]) 
VALUES('5820','ante.Maecenas.mi@pede.co.uk','10-Aug-2016','0','0'),
('32','ante.Maecenas.mi@pede.co.uk','11-Oct-2015','0','0'),
('32','eget@et.ca','12-Nov-2015','0','0'),
('33','eget@et.ca','22-Aug-2016','0','0'),
('32','vestibulum.Mauris@lobortismauris.org','15-Jul-2016','0','0'),
('33','vestibulum.Mauris@lobortismauris.org','02-Nov-2016','0','0'),
('34','vestibulum.Mauris@lobortismauris.org','15-Oct-2016','0','0'),
('35','id.libero.Donec@vitae.edu','27-Nov-2015','0','0'),
('36','id.libero.Donec@vitae.edu','10-Jul-2015','0','0'),
('38','id.libero.Donec@vitae.edu','27-Sep-2015','0','0'),
('40','id.libero.Donec@vitae.edu','25-Aug-2016','0','0'),
('11','lobortis.quis.pede@erategetipsum.edu','03-Jul-2016','0','0'),
('20','lobortis.quis.pede@erategetipsum.edu','30-Sep-2016','0','0'),
('23','libero@gravidanunc.com','10-Mar-2016','0','0'),
('14','libero@gravidanunc.com','07-Jul-2016','0','0'),
('37','libero@gravidanunc.com','11-Sep-2015','0','0'),
('12','Pellentesque.habitant.morbi@pellentesqueegetdictum.org','22-Nov-2015','0','0'),
('16','Pellentesque.habitant.morbi@pellentesqueegetdictum.org','01-Sep-2016','0','0'),
('19','convallis.dolor@Pellentesquetincidunttempus.net','09-May-2016','0','0'),
('44','convallis.dolor@Pellentesquetincidunttempus.net','10-Apr-2015','0','0'),
('37','convallis.dolor@Pellentesquetincidunttempus.net','27-Jan-2016','0','0'),
('25','ultricies.ornare.elit@nisidictumaugue.co.uk','10-Apr-2015','0','0'),
('13','ultricies.ornare.elit@nisidictumaugue.co.uk','30-Sep-2015','0','0'),
('23','dolor@maurisut.net','27-Aug-2015','0','0'),
('33','dolor@maurisut.net','23-Apr-2016','0','0'),
('41','eget@et.ca','11-Sep-2015','0','0'),
('12','eget@et.ca','04-Oct-2015','0','0'),
('27','Mauris@adipiscinglacusUt.co.uk','16-Apr-2015','0','0'),
('26','Mauris@adipiscinglacusUt.co.uk','16-Dec-2015','0','0'),
('13','ultrices@parturient.org','05-May-2016','0','0'),
('18','ultrices@parturient.org','14-Sep-2015','0','0'),
('39','odio.Aliquam@convallisin.net','02-May-2016','0','0'),
('26','odio.Aliquam@convallisin.net','28-Jul-2015','0','0'),
('25','Aliquam.ultrices.iaculis@Aliquam.org','03-Jun-2015','0','0'),
('11','Aliquam.ultrices.iaculis@Aliquam.org','16-Sep-2016','0','0'),
('12','Aliquam.ultrices.iaculis@Aliquam.org','19-Jun-2016','0','0'),
('34','pede@egetdictum.ca','17-Nov-2015','0','0'),
('35','pede@egetdictum.ca','22-Jun-2016','0','0'),
('36','pede@egetdictum.ca','08-Jul-2016','0','0'),
('37','Nulla.aliquet.Proin@lacuspedesagittis.edu','16-Sep-2015','0','0'),
('38','Nulla.aliquet.Proin@lacuspedesagittis.edu','05-May-2016','0','0'),
('29','In@est.com','17-Sep-2015','0','0'),
('39','In@est.com','22-Aug-2015','0','0'),
('40','In@est.com','06-Aug-2015','0','0'),
('41','In@est.com','31-Jan-2016','0','0'),
('42','eu.dui@consequat.com','16-Jul-2015','0','0'),
('43','eu.dui@consequat.com','18-May-2015','0','0'),
('44','eu.dui@consequat.com','27-Apr-2016','0','0'),
('11','euismod.ac.fermentum@turpisvitae.edu','18-Sep-2016','0','0'),
('12','euismod.ac.fermentum@turpisvitae.edu','29-Oct-2016','0','0'),
('13','euismod.ac.fermentum@turpisvitae.edu','08-Mar-2016','0','0'),
('14','felis.adipiscing.fringilla@ametconsectetueradipiscing.ca','02-Dec-2016','0','0'),
('15','felis.adipiscing.fringilla@ametconsectetueradipiscing.ca','03-Dec-2015','0','0'),
('16','tristique@Aliquamultrices.com','18-May-2016','0','0'),
('17','tristique@Aliquamultrices.com','19-May-2016','0','0'),
('18','tristique@Aliquamultrices.com','24-Jan-2016','0','0'),
('19','tristique@Aliquamultrices.com','07-Sep-2016','0','0'),
('20','fames@accumsaninterdum.edu','17-Jan-2016','0','0'),
('21','fames@accumsaninterdum.edu','06-Jan-2016','0','0'),
('22','Aenean@suscipitestac.ca','08-Aug-2015','0','0'),
('23','Aenean@suscipitestac.ca','02-Oct-2016','0','0'),
('24','Aenean@suscipitestac.ca','20-Mar-2016','0','0'),
('25','scelerisque.neque.Nullam@dignissimmagnaa.org','02-Nov-2015','0','0'),
('26','scelerisque.neque.Nullam@dignissimmagnaa.org','27-May-2015','0','0'),
('27','purus@a.org','08-Feb-2016','0','0'),
('28','purus@a.org','12-May-2016','0','0'),
('29','odio@dictummiac.edu','24-Jan-2016','0','0'),
('30','odio@dictummiac.edu','19-Jun-2016','0','0'),
('31','odio@dictummiac.edu','02-Mar-2016','0','0'),
('32','odio@dictummiac.edu','26-Jul-2016','0','0'),
('33','sollicitudin.orci@utlacusNulla.com','27-Oct-2015','0','0'),
('34','sollicitudin.orci@utlacusNulla.com','09-Oct-2016','0','0'),
('35','ante.ipsum@tellusPhaselluselit.edu','02-May-2016','0','0'),
('36','ante.ipsum@tellusPhaselluselit.edu','27-Jul-2016','0','0'),
('37','sed.pede@sed.com','05-Dec-2016','0','0'),
('38','sed.pede@sed.com','11-Aug-2016','0','0'),
('39','sed.pede@sed.com','24-Dec-2015','0','0'),
('40','eget.ipsum.Donec@risusNuncac.com','25-Jan-2016','0','0'),
('41','eget.ipsum.Donec@risusNuncac.com','21-Apr-2015','0','0'),
('42','mattis.ornare@ornaretortor.com','07-May-2015','0','0'),
('43','mattis.ornare@ornaretortor.com','30-Aug-2016','0','0'),
('44','volutpat@Nunc.co.uk','16-Feb-2016','0','0'),
('20','pharetra@fringillaDonec.co.uk','29-May-2016','0','0'),
('11','pharetra@fringillaDonec.co.uk','27-Jun-2016','0','0'),
('12','pharetra@fringillaDonec.co.uk','05-Sep-2015','0','0'),
('31','at.sem@risusvarius.com','15-Aug-2016','0','0'),
('32','at.sem@risusvarius.com','11-Jul-2015','0','0'),
('17','at.sem@risusvarius.com','18-Jul-2016','0','0'),
('18','nibh@accumsan.ca','20-May-2016','0','0'),
('27','nibh@accumsan.ca','12-Nov-2016','0','0'),
('40','nibh.Aliquam.ornare@tincidunt.com','07-Jul-2016','0','0'),
('32','nibh.Aliquam.ornare@tincidunt.com','02-Jun-2016','0','0'),
('33','lorem@Pellentesquetincidunt.ca','20-Oct-2016','0','0'),
('43','lorem@Pellentesquetincidunt.ca','03-Aug-2016','0','0'),
('18','a.nunc@fringilla.com','23-Jul-2016','0','0'),
('11','Phasellus.in.felis@egestas.org','25-May-2016','0','0'),
('23','neque.vitae.semper@arcu.ca','27-Jan-2016','0','0'),
('15','luctus@Vivamusnon.org','03-Jun-2015','0','0'),
('38','luctus@Vivamusnon.org','09-Feb-2016','0','0'),
('19','ut.sem.Nulla@facilisismagnatellus.org','03-Jul-2015','0','0');
