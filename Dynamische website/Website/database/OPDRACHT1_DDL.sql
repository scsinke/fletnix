DROP DATABASE fletnixweb
GO
CREATE DATABASE fletnixweb
GO

DROP TABLE WatchHistory
DROP TABLE Customer
DROP TABLE Country
DROP TABLE Movie_Genre
DROP TABLE Genre
DROP TABLE Movie_Directors
DROP TABLE Movie_Cast
DROP TABLE Person
DROP TABLE Movie

CREATE TABLE Movie
(
	Movie_id			INT 			NOT NULL,
	Title				VARCHAR (255)	NOT NULL, 
	Duration			INT, 
	Description			VARCHAR (MAX), 
	Publication_Year	INT, 
	Cover_Image			VARCHAR (255), 
	Previous_Part		INT, 
	Price				NUMERIC			NOT NULL, 
	URL					VARCHAR (255),

	CONSTRAINT			PK_Movie			PRIMARY KEY (Movie_id) 
)

CREATE TABLE Person
(
	Person_id	INT				NOT NULL, 
	Lastname	VARCHAR (50)	NOT NULL, 
	Firstname	VARCHAR (50)	NOT NULL, 
	Gender		CHAR (1), 

	CONSTRAINT	PK_Person		PRIMARY KEY	(Person_id)
)

CREATE TABLE Movie_Cast
(
	Movie_id	INT				NOT NULL,
	Person_id 	INT				NOT NULL, 
	Role		VARCHAR	(255)	NOT NULL,
	
	CONSTRAINT	PK_Movie_Cast	PRIMARY KEY (Movie_id, Person_id, Role)
)

CREATE TABLE Movie_Directors
(
	Movie_id	INT				NOT NULL, 
	Person_id	INT				NOT NULL, 
	
	CONSTRAINT	PK_Movie_Directors	PRIMARY KEY (Movie_id, Person_id) 
)

CREATE TABLE Genre
(
	Genre_name	VARCHAR	(255)	NOT NULL, 

	CONSTRAINT	PK_Genre_Name	PRIMARY KEY (Genre_Name)
)

CREATE TABLE Movie_Genre
(
	Movie_id	INT				NOT NULL, 
	Genre_Name	VARCHAR (255)	NOT NULL, 

	CONSTRAINT	PK_Movie_Genre	PRIMARY KEY (Movie_id, Genre_Name) 
)

CREATE TABLE Country
(
	Country_name	VARCHAR (50)	NOT NULL, 

	CONSTRAINT		PK_Country_Name PRIMARY KEY (Country_Name)
)

CREATE TABLE Customer
(
	Customer_Mail_Address	VARCHAR (255)	NOT NULL, 
	Name					VARCHAR (255)	NOT NULL, 
	Paypal_Account			VARCHAR (255)	NOT NULL, 
	Subscription_Start		DATE			NOT NULL, 
	Subscription_End		DATE, 
	Password				VARCHAR (MAX)	NOT NULL, 
	Country_Name			VARCHAR (50)	NOT NULL, 

	CONSTRAINT				PK_Customer_Mail_Address	PRIMARY KEY (Customer_Mail_Address) 
)

CREATE TABLE WatchHistory
(
	Movie_id				INT				NOT NULL, 
	Customer_Mail_Address	VARCHAR (255)	NOT NULL, 
	Watch_Date				DATETIME			NOT NULL, 
	Price					NUMERIC (5,2)	NOT NULL, 
	Invoiced				BIT				NOT NULL, 

	CONSTRAINT				PK_WatchHistory				PRIMARY KEY (Movie_id, Customer_Mail_Address, Watch_Date)
)

ALTER TABLE Person
	ADD CONSTRAINT CK_Gender 
		CHECK (Gender IN ('M' , 'F'))

ALTER TABLE Movie
	ADD CONSTRAINT CK_Publication_Year
		CHECK (Publication_Year > 1890 AND Publication_Year <= 2016)

ALTER TABLE Customer
	ADD CONSTRAINT	CK_Subscription_Start
		CHECK (Subscription_Start < Subscription_End)

ALTER TABLE Customer
	ADD CONSTRAINT	UK_PayPal_Account
		UNIQUE (Paypal_Account)
GO

/*
Drie extra Constraints die voor de hand liggen.
	1. de Price Column in Movie mag niet gelijk zijn aan 0.
	2. de Price Column in WatchHistory mag niet gelijk zijn aan 0.
	3. de Country_Name Column in Country moet Uniek zijn.
*/

ALTER TABLE Movie
	ADD CONSTRAINT CK_Price_Movie
		CHECK	(Price >= 0)

ALTER TABLE WatchHistory
	ADD CONSTRAINT CK_Price_WatchHistory
		CHECK	(Price >= 0)

ALTER TABLE Country
	ADD CONSTRAINT UK_Country_Name
		UNIQUE (Country_Name)


/*Update en Delete Regels voor Beinvloede tabel*/

ALTER TABLE Movie
	ADD CONSTRAINT FK_Previous_Part	
		FOREIGN KEY (Previous_Part)	REFERENCES Movie(Movie_id)
		ON UPDATE NO ACTION
		ON DELETE NO ACTION

ALTER TABLE Movie_Cast
	ADD CONSTRAINT  FK2_Movie_id	
		FOREIGN KEY	(Movie_id)	REFERENCES Movie(Movie_id)
		ON UPDATE CASCADE
		ON DELETE CASCADE, 	
	
		CONSTRAINT  FK_Person_id
		FOREIGN KEY (Person_id)	REFERENCES Person(Person_id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
		
ALTER TABLE Movie_Directors
	ADD CONSTRAINT	FK3_Movie_id		
		FOREIGN KEY (Movie_id)	REFERENCES Movie(Movie_id)
		ON UPDATE CASCADE
		ON DELETE CASCADE, 
	
		CONSTRAINT	FK2_Person_id		
		FOREIGN KEY (Person_id)	REFERENCES Person(Person_id)
		ON UPDATE CASCADE
		ON DELETE CASCADE  	

ALTER TABLE Movie_Genre
	ADD CONSTRAINT 	FK4_Movie_id	
		FOREIGN KEY (Movie_id)		REFERENCES Movie(Movie_id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,  
	
		CONSTRAINT	FK_Genre_Name	
		FOREIGN KEY	(Genre_name)	REFERENCES Genre(Genre_name)
		ON UPDATE CASCADE
		ON DELETE CASCADE 

ALTER TABLE Customer
	ADD CONSTRAINT	FK_Country_Name				
		FOREIGN KEY (Country_Name)	REFERENCES Country(Country_Name)
		ON UPDATE CASCADE
		ON DELETE CASCADE

ALTER TABLE WatchHistory
	ADD CONSTRAINT	FK5_Movie_id				
		FOREIGN KEY (Movie_id)				REFERENCES Movie(Movie_id)
		ON UPDATE CASCADE
		ON DELETE CASCADE, 
	
		CONSTRAINT	FK_Customer_Mail_Address	
		FOREIGN KEY (Customer_Mail_Address) REFERENCES Customer(Customer_Mail_Address)
		ON UPDATE CASCADE
		ON DELETE CASCADE