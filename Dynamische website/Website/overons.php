<!DOCTYPE html>
<html>
<head>
	<title>FLETNIX: Over ons</title>
	<link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body>
	<header>
		<?php
        require 'functions.php';
        $page = 'overons';
        include 'header.php'; ?>
	</header>
    <main class="container">
        <h1>Over FLETNIX</h1>
        <div class="one_half">
            <p>Fletnix is een nieuw video-on-demand internetplatform. Wij bieden aangekochte &eacute;n zelf geproduceerde films en series voor alle leeftijden.</p>
            <p>Bij ons betaal je een vast bedrag per maand om onbeperkt films en series te bekijken. Wat je kunt bekijken en in welke kwaliteit hangt af van <a href="registreren.php">welk pakket je kiest</a>.</p>
        </div><div class="one_half last">
            <p>Ons basis pakket is voor klanten die niet te veel geld uit willen geven. Je kunt hiermee alle content bekijken, behalve het door ons zelf geproduceerde materiaal. Dit abonnement is per jaar opzegbaar.</p>
            <p>Wil je dit in HD kunnen bekijken, hebben we het standaard pakket voor je. Deze is per maand opzegbaar. Voor de echte Fletnix fans hebben we ook het premium pakket, waarbij je ook onze originals kunt bekijken!</p>
        </div>
    </main>
    <footer>
		<?php include 'footer.php'; ?>
	</footer>
</body>
</html>