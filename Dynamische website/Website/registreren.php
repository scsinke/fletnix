<!DOCTYPE html>
<html>
<head>
	<title>FLETNIX: Registreren</title>
	<link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body>
	<header>
		<?php
        require 'functions.php';
        $page = 'registreren';
        include 'header.php'; ?>
	</header>
    <section id="registration">
		<div class="container">
			<h2>Kies een van onze pakketten</h2>
			<div id="wrapper">
				<div id="pakket_1" class="pakketten">
					<h3>Basic</h3>
                    <ul>
                        <li>Onbeperkt streamen</li>
                        <li>Per jaar opzegbaar</li>
                        <li class="price">&euro;7,50 p.m.</li>
                    </ul>
                    <a href="#registreren">registreer</a>
				</div>
				<div id="pakket_2" class="pakketten">
					<h3>Standaard</h3>
                    <ul>
                        <li>Alles in Full HD</li>
                        <li>Per maand opzegbaar</li>
                        <li class="price">&euro;9,50 p.m.</li>
                    </ul>
                    <a href="#registreren">registreer</a>
				</div>
				<div id="pakket_3" class="pakketten">
					<h3>Premium</h3>
                    <ul>
                        <li>Extra Fletnix content</li>
                        <li>Per maand opzegbaar</li>
                        <li class="price">&euro;11,00 p.m.</li>
                    </ul>
                    <a href="#registreren">registreer</a>
				</div>
			</div>
		</div>
	</section>
	<section id="over_ons">
    <main class="container">
        <a id="registreren"></a>
        <h1>Registreren</h1>

        <?php

        if($_SERVER['REQUEST_METHOD']=='POST') {
            if(valideerRegistratieForm()!=''){
                toonRegistratieForm();
            } else {
                valideerRegistratieForm();
                invoerCustomerInDB($_POST["userEmail"], $_POST["fullName"], $_POST["paypalAccount"], $_POST["password"], $_POST["country"]);
                registratieGelukt();
            }
        } else {
            toonRegistratieForm();
        }
        ?>

    </main>
    </section>
    <footer>
		<?php include 'footer.php'; ?>
	</footer>
</body>
</html>