<!DOCTYPE html>
<html>
<head>
	<title>FLETNIX: Filmpagina</title>
	<link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body>
	<header>
		<?php
        require 'functions.php';
        $page = 'videoplayer';
        include 'header.php';
        $movie_info_array = get_movie_info($_GET["movie"]); 
        $movie_genre_array = movie_genre($_GET["movie"]);
        $movie_cast_array = movie_cast($_GET["movie"]);
        $movie_directors_array = movie_directors($_GET["movie"]);
        update_watch_history($_GET["movie"],$_SESSION['gebruiker_gegevens']['Customer_Mail_Address']);
        ?>
    </header>
    <main class="container">
        <?php
        if (array_key_exists('gebruikersnaam',$_SESSION)) {
        ?>
            <h1>Film bekijken</h1>
            <div id="video">
                <div id="film">
                    <div id="film_image">
                    <?php
                    foreach ($movie_info_array as $movie_info) {
                        echo "<img alt='cover_foto' src='images/$movie_info[Cover_Image].jpg'>";
                    }    
                    ?>
                    </div>
                    <div id="film_info">
                        <div>
                        <?php
                        foreach ($movie_info_array as $movie_info) {
                            echo "<h1>$movie_info[Title]<sup>$movie_info[Publication_Year]</sup></h1>";
                        } 
                        ?>  
                        </div>
                        <?php
                        foreach ($movie_info_array as $movie_info) {
                            echo "<p>$movie_info[Description]</p>";
                        } 
                        ?>       
                    </div>
                    <div id="genre_cast_directors">
                        <ul class="genres">
                        <?php
                            foreach ($movie_genre_array as $movie_genre) {
                              echo "<li>$movie_genre[Genre_Name]</li>";
                            }
                        ?>
                        </ul>
                        <p><strong>STARS</strong></p>
                        <ul class="persons">
                        <?php
                            foreach ($movie_cast_array as $movie_cast) {
                                echo "<li>". $movie_cast['firstname'] ." ". $movie_cast['lastname'] ."</li>";
                            }
                        ?></ul>
                        <p><strong>DIRECTORS</strong></p>
                        <ul class="persons">
                        <?php
                            foreach ($movie_directors_array as $movie_directors) {
                                echo "<li>". $movie_directors['firstname'] ." ". $movie_directors['lastname'] ."</li>";
                            }
                        ?></ul>
                    </div>
                </div>
                <div>
                <?php
                foreach ($movie_info_array as $movie_info) {
                    echo "<iframe src='$movie_info[URL]' allowfullscreen style='width: 100%; height: 563px;'>
                    </iframe>";
                }
                ?>
                </div>
            </div>
        <?php } else {
            echo "<h1>Film bekijken</h1>";
            echo "<p>Je moet <a href=\"login.php\">inloggen</a> om deze pagina te kunnen bekijken.</p>";
        } ?>
    </main>
    <footer>
		<?php include 'footer.php'; ?>
	</footer>
</body>
</html>