<?php

/*
 * Connectie maken met database
 */
ini_set('display_errors', 1);
$dbHostname = "localhost"; //Naam van de Server
$dbName = "fletnixweb";    //Naam van de Database
$dbUsername = "sa";      //Inlognaam
$dbPassword = "esterade";      //Password
global $pdo;

$pdo = new PDO ("sqlsrv:Server=$dbHostname;Database=$dbName", "$dbUsername", "$dbPassword");
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Inlog formulier function
function show_form($errors=array())
{
        $errorHtml = '<ul><li>';
        $errorHtml .= implode('</li><li>', $errors);
        $errorHtml .= '</li></ul>';
    
    echo <<<_FORM_
<form method="POST" action="$_SERVER[PHP_SELF]">$errorHtml
<fieldset>Gebruikersnaam: <input type="text" name="gebruikersnaam" value="$GLOBALS[gebruikersnaam]"></fieldset>
<fieldset>Wachtwoord: <input type="password" name="wachtwoord" value=""></fieldset>
<input type="submit" name="submit" value="inloggen">
</form>
_FORM_;
}

// Inlog formulier validatie
function validate_form()
{
    $input = array();
    $errors = array();

    // Zet de gebruikers uit de database in de array $users
    $data = $GLOBALS['pdo']->query("SELECT Customer_Mail_Address, Password FROM Customer");
    $users = array();
    while ($row = $data->fetch()){
        $key = $row['Customer_Mail_Address'];
        $value = $row['Password'];
        $users[$key] = $value;
    }

    // Controleer gebruikersnaam
    $input['gebruikersnaam'] = $_POST['gebruikersnaam'] ?? '';
    if (! array_key_exists($input['gebruikersnaam'], $users)) {
        $errors[] = 'Voer een juiste gebruikersnaam in';
    } else {
        //Controleer wachtwoord
        $saved_password = $users[$input['gebruikersnaam']];
        $submitted_password = $_POST['wachtwoord'] ?? '';
        if (!password_verify($submitted_password, $saved_password)) {
            $errors[] = 'Voer een juist wachtwoord in';
        }
    }
    return array($errors, $input);
}

// Gebruikersnaam meegeven aan session
function process_form($input) {
    $huidigeGebruiker = $input['gebruikersnaam'];
    $_SESSION['gebruikersnaam'] = $huidigeGebruiker;
    echo "Welkom terug, ".$_SESSION['gebruikersnaam']."<br>";
    echo "<a href=\"dashboard.php\">Ga naar je persoonlijk filmoverzicht >></a>";

    $data = $GLOBALS['pdo']->query("SELECT Customer_Mail_Address, Name, Paypal_Account, Subscription_Start, Subscription_End, Password, Country_Name FROM Customer WHERE Customer_Mail_Address = '$huidigeGebruiker'");
    $gebruikerInfo = array();
    while ($row = $data->fetch()){
        $gebruikerInfo['Customer_Mail_Address'] = $row['Customer_Mail_Address'];
        $gebruikerInfo['Name'] = $row['Name'];
        $gebruikerInfo['Paypal_Account'] = $row['Paypal_Account'];
        $gebruikerInfo['Subscription_Start'] = $row['Subscription_Start'];
        $gebruikerInfo['Subscription_End'] = $row['Subscription_End'];
        $gebruikerInfo['Country_Name'] = $row['Country_Name'];
    }

    $_SESSION['gebruiker_gegevens'] = $gebruikerInfo;
}

// Registratie formulier validatie
function valideerRegistratieForm()
{
    global $message;
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            $message = ucwords($key) . " field is required";
            break;
        }
    }
    /* Password Matching Validation */
    if ($_POST['password'] != $_POST['confirm_password']) {
        $message = 'Passwords should be same<br>';
    }

    /* Email Validation */
    if (!isset($message)) {
        if (!filter_var($_POST["userEmail"], FILTER_VALIDATE_EMAIL)) {
            $message = "Invalid UserEmail";
        }
    }

    /* Paypal Email Validation */
    if (!isset($message)) {
        if (!filter_var($_POST["paypalAccount"], FILTER_VALIDATE_EMAIL)) {
            $message = "Invalid paypal account";
        }
    }

    /* Validation to check if country is selected */
    if (!isset($message)) {
        if (!isset($_POST["country"])) {
            $message = "Country field is required";
        }
    }

    /* Validation to check if abonnement is selected */
    if (!isset($message)) {
        if (!isset($_POST["abonnement"])) {
            $message = "Abonnement field is required";
        }
    }

    echo "<div class=\"message\">". $message ."</div>";
    return $message;
}

// Toon registratie formulier
function toonRegistratieForm(){ ?>

    <form name="registreren" method="post">
        <table>
            <tr><td>Emailadres</td>
                <td><input type="text" class="inputBox" name="userEmail" value="<?php if(isset($_POST['userEmail'])) echo $_POST['userEmail']; ?>"></td>
            </tr>
            <tr><td>Voor- en achternaam</td>
                <td><input type="text" class="inputBox" name="fullName" value="<?php if(isset($_POST['fullName'])) echo $_POST['fullName']; ?>"></td>
            </tr>
            <tr><td>Abonnement</td>
                <td><input type="radio" name="abonnement" value="basic" <?php if(isset($_POST['abonnement']) && $_POST['abonnement']=="basic") { ?>checked<?php  } ?>> Fletnix Basic
                    <input type="radio" name="abonnement" value="standaard" <?php if(isset($_POST['abonnement']) && $_POST['abonnement']=="standaard") { ?>checked<?php  } ?>> Fletnix Standaard
                    <input type="radio" name="abonnement" value="premium" <?php if(isset($_POST['abonnement']) && $_POST['abonnement']=="premium") { ?>checked<?php  } ?>> Fletnix Premium

                </td>
            </tr>
            <tr><td>Land</td>
                <td><select class="selectBox" name="country">
                        <option value="Netherlands">Nederland</option>
                        <option value="Belgium">Belgie</option>
                        <option value="United Kingdom">Verenigd Koninkrijk</option>
                        <option value="Germany">Duitsland</option>
                        <option value="Sweden">Zweden</option>
                    </select></td>
            </tr>
            <tr><td>Wachtwoord</td>
                <td><input type="password" class="inputBox" name="password" value=""></td>
            </tr>
            <tr><td>Herhaal wachtwoord</td>
                <td><input type="password" class="inputBox" name="confirm_password" value=""></td>
            </tr>
            <tr><td>Paypal account (emailadres)</td>
                <td><input type="text" class="inputBox" name="paypalAccount" value="<?php if(isset($_POST['paypalAccount'])) echo $_POST['paypalAccount']; ?>"></td>
            </tr>
        </table>
        <div><input type="submit" name="submit" value="Register" class="btnRegister"></div>
    </form>
<?php }

// Schrijf registratie van gebruiker naar database
function invoerCustomerInDB($customer_mail_address, $name, $paypal_account, $password, $country_name) {
    global $pdo;
    $hashedPassword = password_hash($password,1);
    try {
        $stmt = $pdo->prepare("INSERT INTO Customer (Customer_Mail_Address, Name, Paypal_Account, Subscription_Start, Subscription_End, Password, Country_Name) VALUES (?,?,?,?,?,?,?)");
        $stmt->execute(array($customer_mail_address, $name, $paypal_account, date('Ymd'), NULL, $hashedPassword, $country_name));
    } catch (PDOException $e) {
        echo "Could not insert user, ".$e->getMessage();
    }
}

// Geregistreerde gebruikers weergeven
function toonCustomersInDB() {
    global $pdo;
    try {
        $data = $pdo->query("SELECT * FROM Customer");
        while ($row = $data->fetch()){
            echo "$row[Customer_Mail_Address] --> $row[Password]</br>";
        }
    } catch (PDOException $e) {
        echo "Could not read users, ".$e->getMessage();
    }
}

function registratieGelukt(){
     echo "Registratie gelukt! <a href=\"login.php\">Klik hier om meteen in te loggen >></a>";
}

// Dit haalt de films op uit de database
function get_movies():array{
    global $pdo;
    try{
        $movie_data = array();
        $data=$pdo->query("SELECT M.Movie_id,M.Title,M.Cover_Image FROM Movie M");

        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
            array_push($movie_data, $row);
        }
        return $movie_data;

    } catch (Exception $e) {
        echo $e->getMessage();
        return array();
    }
}

// Dit haalt de movie info voor de videoplayer.php
function get_movie_info(int $movieid):array{
    global $pdo;
    try{
        $movie_info = array();
        $data=$pdo->query("SELECT M.Title,M.Duration,M.Description,M.Publication_Year,M.Cover_Image,M.URL FROM Movie M WHERE Movie_id = $movieid");

        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($movie_info, $row);
            }
        return $movie_info;
    } catch (Exception $e) {
        echo $e->getMessage();
        return array();
    }
}

// Dit is de zoekfunctie
function search_movie(string $search):array
{
    global $pdo;
    try {
        $search_result = array();
        $data = $pdo->query("SELECT M.Movie_id,M.Title,M.Cover_Image FROM Movie M WHERE M.Title LIKE '%$search%'");
        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($search_result, $row);
            }
        return $search_result;
    } catch (Exception $e) {
        echo $e->getMessage();
        return array();
    }
}

// Genre_info voor films in videoplayer.php
function movie_genre(int $movieid):array{
    global $pdo;
    try{
        $genre_results = array();
        $data = $pdo->query("SELECT MG.Genre_Name FROM Movie_Genre MG WHERE MG.Movie_id = $movieid");

        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($genre_results, $row);
            }  
        return $genre_results;

    } catch(Exception $e){
        echo $e->getMessage();
        return array();
    }
}

// Film cast
function movie_cast(int $movieid):array{
    global $pdo;
    try{
        $cast_result = array();
        $data = $pdo->query("SELECT P.firstname, P.lastname FROM Person P, Movie M, Movie_Cast MC WHERE M.movie_id = $movieid AND M.movie_id = MC.movie_id AND MC.person_id = P.person_id");

        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($cast_result, $row);
            }
        return $cast_result;
    } catch(Exception $e){
        echo $e->getMessage();
        return array();
    }
}

// Film directors
function movie_directors(int $movieid):array{
    global $pdo;
    try{
        $directors_result = array();
        $data = $pdo->query("SELECT P.firstname, P.lastname FROM Person P, Movie M, Movie_Directors MD WHERE M.movie_id = $movieid AND M.movie_id = MD.movie_id AND MD.person_id = P.person_id");

        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($directors_result, $row);
            }
        return $directors_result;
    } catch(Exception $e){
        echo $e->getMessage();
        return array();
    }
}

// Meest bekeken films
function top_movies():array{
    global $pdo;
    try{
        $top_movies = array();
        $data = $pdo->query("SELECT TOP 6 M.Movie_id, M.Title, M.Cover_Image FROM Movie M WHERE M.Movie_id IN (SELECT movie_id FROM WatchHistory) ORDER BY (COUNT(movie_id) OVER(PARTITION BY movie_id)) DESC");

        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($top_movies, $row);
            }
        return $top_movies;
    } catch(Exception $e){
        echo $e->getMessage();
        return array();
    }
}

// Recent bekeken films
function recent_movies($customer_mail_address):array{
    global $pdo;
    try{
        $recent_movies = array();
        $data = $pdo->query("SELECT TOP 6 M.Movie_id, M.Title, M.Cover_Image FROM Movie M WHERE movie_id IN (SELECT WH.movie_id FROM WatchHistory WH WHERE WH.customer_mail_address = '$customer_mail_address')");
        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($recent_movies, $row);
            }
        return $recent_movies;
    } catch(Exception $e){
        echo $e->getMessage();
        return array();
    }
}

// Populaire films
function popular_movies():array{
    global $pdo;
    try{
        $popular_movies = array();
        $data = $pdo->query("SELECT TOP 6 M.Movie_id, M.Title, M.Cover_Image FROM Movie M WHERE movie_id IN (SELECT TOP 10 WH.movie_id FROM WatchHistory WH WHERE watch_date < GETDATE() AND watch_date > DATEADD(ww, -2, GETDATE()) GROUP BY movie_id ORDER BY COUNT(WH.movie_id) OVER(PARTITION BY movie_id) DESC)");
        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($popular_movies, $row);
            }
        return $popular_movies;
    } catch(Exception $e){
        echo $e->getMessage();
        return array();
    }
}

// Onlangs toegevoegde films
function recent_added_movies():array{
    global $pdo;
    try{
        $recent_added = array();
        $data = $pdo->query("SELECT TOP 6 M.Movie_id, M.Title, M.Cover_Image FROM Movie M ORDER BY Publication_Year DESC");
        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                array_push($recent_added, $row);
            }
        return $recent_added;
    } catch(Exception $e){
        echo $e->getMessage();
        return array();
    }
}

// Watchhistory updaten 
function update_watch_history(int $movieid, string $customer_mail_address){
    global $pdo;
    try {
        $stmt = $pdo->prepare("INSERT INTO WatchHistory (Movie_id,Customer_Mail_Address,Watch_Date,Price,Invoiced) VALUES (?,?,?,?,?)");
        $stmt->execute(array($movieid, $customer_mail_address, date('Ymd h:i:s a'), 2, 0));
    } catch (PDOException $e) {
        echo "Could not insert user, ".$e->getMessage();
    }
}

?>