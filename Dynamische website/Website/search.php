<!DOCTYPE html>
<html>
<head>
    <title>FLETNIX: Overzicht</title>
    <link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body>
    <header>
        <?php
        require 'functions.php';
        $page = 'browse_films';
        include 'header.php';
        ?>
    </header>
    <main class="container">
        <?php
        if (array_key_exists('gebruikersnaam',$_SESSION)) {
        ?>
            <?php
                if($_GET != NULL){ ?>
                    <h1>Zoek resultaten voor "<?php echo $_GET['query'] ?>"</h1>
                    <div class="overzicht">
                        <?php
                        $movie_search = search_movie($_GET["query"]);
                        foreach ($movie_search as $movie_info) {
                            echo "<a class='product' href='videoplayer.php?movie=$movie_info[Movie_id]'><img alt='cover' src='images/$movie_info[Cover_Image].jpg'><h3>$movie_info[Title]</h3></a>";
                        }
                    echo "</div>";
                } else {
                    
                    echo "<h1>Geen zoek resultaten gevonden</h1>
                    <div class=\"overzicht\"><p>U heeft geen zoekopdracht verstuurd. Ga terug naar het <a href=\"browse_films.php\">film overzicht >></a></p></div>";                        
                }                        
             ?>
            
         <?php } else {
            echo "<h1>Film overzicht</h1>";
            echo "<p>Je moet <a href=\"login.php\">inloggen</a> om deze pagina te kunnen bekijken.</p>";
        } ?>
    </main>
    <footer>
        <?php include 'footer.php'; ?>
    </footer>
</body>
</html>