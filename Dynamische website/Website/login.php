<!DOCTYPE html>
<html>
<head>
	<title>FLETNIX: Overzicht</title>
	<link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body>
	<header>
		<?php
        require 'functions.php';
        $page = 'login';
        include 'header.php'; ?>
	</header>
    <main class="container">
        <h1>Inloggen</h1>
        <?php
        $gebruikersnaam = '';
        if ($_SERVER['REQUEST_METHOD']=='POST') {
            list($errors, $input) = validate_form();
            if ($errors) {
                show_form($errors);
            } else {
                process_form($input);
            }
        } else {
            show_form();
        }

        echo "<br><br>";
        ?>
    </main>
    <footer>
		<?php include 'footer.php'; ?>
	</footer>
</body>
</html>