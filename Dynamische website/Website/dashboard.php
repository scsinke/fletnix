<!DOCTYPE html>
<html>
<head>
	<title>Fletnix: Dashboard</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
	<header>
        <?php
        require 'functions.php';
        $page = 'dashboard';
        include 'header.php';
        ?>
	</header>
        <?php
            if (array_key_exists('gebruikersnaam',$_SESSION)) {
                echo "<div class=\"container\"><h1>Dashboard van ".$_SESSION['gebruiker_gegevens']['Name']."</h1></div>";
                ?>
    <section id="volgend">
        <div class="container">
            <h2>Recent toegevoegd</h2>
            <div class="overzicht">
                <?php
                $recent_toegevoegd_array = recent_added_movies();
                foreach ($recent_toegevoegd_array as $movie) {
                    echo "<a class='product' href='videoplayer.php?movie=$movie[Movie_id]'><img alt='cover' src='images/$movie[Cover_Image].jpg'><h3>$movie[Title]</h3></a>";
                }
                ?>
            </div>
        </div>
    </section>
    <section id="top_films">
        <div class="container">
            <h2>Top films</h2>
            <div class="overzicht">
                <?php
                $movie_array = top_movies();
                foreach ($movie_array as $movie) {
                    echo "<a class='product' href='videoplayer.php?movie=$movie[Movie_id]'><img alt='cover' src='images/$movie[Cover_Image].jpg'><h3>$movie[Title]</h3></a>";
                }
                ?>			
            </div>
        </div>
    </section>
    <section id="aangeraden_films">
        <div class="container">
            <h2>Film aanraders</h2>
            <div class="overzicht">
                <?php
                $popular_movies_array = popular_movies();
                foreach ($popular_movies_array as $movie) {
                    echo "<a class='product' href='videoplayer.php?movie=$movie[Movie_id]'><img alt='cover' src='images/$movie[Cover_Image].jpg'><h3>$movie[Title]</h3></a>";
                }
                ?>
            </div>
        </div>
    </section>
    <section id="aangeraden_series">
        <div class="container">
            <h2>Recent bekeken</h2>
            <div class="overzicht">
            <!--moet nog naar gekeken worden ivm customer_mail_address-->
                 <?php
                $recent_movies_array = recent_movies($_SESSION['gebruiker_gegevens']['Customer_Mail_Address']);
                foreach ($recent_movies_array as $movie) {
                    echo "<a class='product' href='videoplayer.php?movie=$movie[Movie_id]'><img alt='cover' src='images/$movie[Cover_Image].jpg'><h3>$movie[Title]</h3></a>";
                }
                ?>
            </div>
        </div>
    </section>
    <?php } else {
        echo "<div class=\"container\"><h1>Dashboard</h1>";
        echo "<p>Je moet <a href=\"login.php\">inloggen</a> om deze pagina te kunnen bekijken.</p><br><br></div>";
    }
    ?>
    <footer>
        <?php include 'footer.php'; ?>
	</footer>
</body>
</html>