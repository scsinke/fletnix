<?php session_start(); ?>
<div id="content">
    <div class="container">
        <div class="top">
            <a href="dashboard.php">
                <img class="logo" alt="logo" src="images/fletnix-logo.png">
            </a>
            <nav class="top">
                <?php if ($page == 'account'){ ?>
                    <a class="parent" href="index.php">Uitloggen</a>
                <?php } else { ?>
                    <a class="parent" href="account.php">Account</a>
                    <ul class="dropdown">
                        <li><a href="account.php">Accountinformatie</a></li>
                        <li><a href="index.php">Uitloggen</a></li>
                    </ul>
                <?php } ?>
            </nav>
            <nav id="main">
                <a href="dashboard.php" <?php echo ($page == 'dashboard') ? 'class="current"' : '';?>>Home</a>
                <a href="browse_films.php" <?php echo ($page == 'browse_films') ? 'class="current"' : '';?>>Films</a>
                <a href="overons.php" <?php echo ($page == 'overons') ? 'class="current"' : '';?>>Over ons</a>
            </nav>
        </div>
    </div>
</div>