<!DOCTYPE html>
<html>
<head>
    <title>FLETNIX: Overzicht</title>
    <link rel="stylesheet" type="text/css" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body>
    <header>
        <?php
        require 'functions.php';
        $page = 'browse_films';
        include 'header.php';
        ?>
    </header>
    <main class="container">
        <?php
        if (array_key_exists('gebruikersnaam',$_SESSION)) {
            ?>
         <h1>Film overzicht</h1>
        <form method="get" action="search.php" id="searchform"> 
             <input type="text" name="query" placeholder="Zoek een film..."> 
             <input type="submit" name="submit" value="ga"> 
         </form> 
         <div class="overzicht">
            <?php
            $movie_array = get_movies();
            foreach ($movie_array as $movie) {
                echo "<a class='product' href='videoplayer.php?movie=$movie[Movie_id]'><img alt='cover' src='images/$movie[Cover_Image].jpg'><h3>$movie[Title]</h3></a>";
            }
            ?>
        </div>
        <?php } else {
            echo "<h1>Film overzicht</h1>";
            echo "<p>Je moet <a href=\"login.php\">inloggen</a> om deze pagina te kunnen bekijken.</p>";
        } ?>
    </main>
    <footer>
        <?php include 'footer.php'; ?>
    </footer>
</body>
</html>