DELETE FROM Movie_Directors
DELETE FROM Movie_Cast
DELETE FROM Movie_Genre
DELETE FROM Genre
DELETE FROM WatchHistory
DELETE FROM Customer
DELETE FROM Country
DELETE FROM Person
DELETE FROM Movie

INSERT INTO Person
VALUES	(1, 'Wachowski', 'Lana',  'F'),
		(2,'Wachowski', 'Lilly', 'F'),
		(3,'Reeves','Keanu','M'),
		(4,'Fishburne','Laurence','M'),
		(5,'Moss','Carrie-Anne','F'),
		(6,'Coppola','Francis Ford','M'),
		(7,'Brando','Marlon','M'),
		(8,'Pacino','Al','M'),
		(9,'Caan','James','M'),
		(10,'De Niro','Robert','M'),
		(11,'Duvall','Robert','M'),
		(12,'Nolan','Christopher','M'),
		(13,'Bale','Christian','M'),
		(14,'Ledger','Heath','M'),
		(15,'Eckhart','Aaron','M'),
		(16,'Lumet','Sidney','M'),
		(17,'Fonda','Henry','M'),
		(18,'Cobb','Lee J.','M'),
		(19,'Balsam','Martin','M'),
		(20,'Tarantino','Quentin','M'),
		(21,'Travolta','John','M'),
		(22,'Thurman','Uma','F'),
		(23,'Jackson','Samuel L.','M'),
		(24,'Hardy','Tom','M'),
		(25,'Hathaway','Anne','F'),
		(26,'DiCaprio','Leonardo','M'),
		(27,'Gordon-Levitt','Joseph','M'), 
		(28,'Page','Ellen','F'),
		(29,'Aronofsky','Darren','M'),
		(30,'Portman','Natalie','F'),
		(31,'Kunis','Mila','F'),
		(32,'Cassel','Vincent','M'),
		(33,'Lucas','Jon','M'),
		(34,'Moore','Scott','M'),
		(35,'Hahn','Kathryn','F'),
		(36,'Bell','Kristen','F'),
		(37,'Miller','Tim','M'),
		(38,'Reynolds','Ryan','M'),
		(39,'Baccarin','Morena','F'),
		(40,'Miller','T.J.','M'),
		(41,'Derrickson','Scott','M'),
		(42,'Cumberbatch','Benedict','M'),
		(43,'Ejiofor','Chiwetel','M'),
		(44,'McAdams','Rachel','F'),
		(45,'Yates','David','M'),
		(46,'Redmayne','Eddie','M'),
		(47,'Waterston','Katherine','F'),
		(48,'Sudol','Alison','F'),
		(49,'McConaughey','Matthew','M'),
		(50,'Chastain','Jessica','F'),
		(51,'Greengrass','Paul','M'),
		(52,'Damon','Matt','M'),
		(53,'Lee Jones','Tommy','M'),
		(54,'Vikander','Alicia','F'),
		(55,'Spielberg','Steven','M'),
		(56,'Day-Lewis','Daniel','M'),
		(57,'Field','Sally','F'),
		(58,'Strathairn','David','M'),
		(59,'Scorsese','Martin','M'),
		(60,'Mortimer','Emily','F'),
		(61,'Ruffalo','Mark','M'),
		(62,'Jackson','Peter','M'),
		(63,'Freeman','Martin','M'),
		(64,'McKellen','Ian','M'),
		(65,'Armitage','Richard','M'),
		(66,'Scott','Ridley','M'),
		(67,'Wiig','Kristen','F'),
		(68,'I��rritu','Alejandro G.','M'),
		(69,'Poulter','Will','M')

INSERT INTO Movie 
VALUES	(11, 'Matrix, The', 122, 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.', 1999,'the_matrix', NULL, 2.50, 'https://www.youtube-nocookie.com/embed/m8e-FF8MsqU?rel=0&amp;showinfo=0'),
		(12, 'Matrix Reloaded, The', 106, 'Neo and the rebel leaders estimate that they have 72 hours until 250,000 probes discover Zion and destroy it and its inhabitants. During this, Neo must decide how he can save Trinity from a dark fate in his dreams.', 2003, 'the_matrix_reloaded', 11, 2.50, 'https://www.youtube-nocookie.com/embed/kYzz0FSgpSU?rel=0&amp;showinfo=0'),
		(13, 'Matrix Revolutions, The', 103, 'The human city of Zion defends itself against the massive invasion of the machines as Neo fights to end the war at another front while also opposing the rogue Agent Smith.', 2003, 'the_matrix_revolutions', 12, 2.50, 'https://www.youtube-nocookie.com/embed/hMbexEPAOQI?rel=0&amp;showinfo=0'),
		(14, 'Godfather, The', 175, 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.', 1972, 'the_godfather', NULL, 2.00, 'https://www.youtube-nocookie.com/embed/sY1S34973zA?rel=0&amp;showinfo=0'),
		(15, 'Godfather: Part II, The', 202, 'The early life and career of Vito Corleone in 1920s New York is portrayed while his son, Michael, expands and tightens his grip on his crime syndicate stretching from Lake Tahoe, Nevada to pre-revolution 1958 Cuba.', 1974,'the_godfather_part_II', 14, 2.20, 'https://www.youtube-nocookie.com/embed/9O1Iy9od7-A?rel=0&amp;showinfo=0'),
		(16, 'Dark Knight, The', 152, 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.', 2008, 'the_dark_knight', NULL, 2.50, 'https://www.youtube-nocookie.com/embed/EXeTwQWrcwY?rel=0&amp;showinfo=0'),
		(17, '12 Angry Men', 96, 'A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.', 1957, '12_angry_men', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/fSG38tk6TpI?rel=0&amp;showinfo=0'),
		(18, 'Pulp Fiction', 154,'The lives of two mob hit men, a boxer, a gangster''s wife, and a pair of diner bandits intertwine in four tales of violence and redemption.',1994,'pulp_fiction',NULL,1.50,'https://www.youtube-nocookie.com/embed/ewlwcEBTvcg?rel=0&amp;showinfo=0'),
		(19, 'The Dark Knight Rises', 164,'Eight years after the Joker''s reign of anarchy, the Dark Knight, with the help of the enigmatic Selina, is forced from his imposed exile to save Gotham City, now on the edge of total annihilation, from the brutal guerrilla terrorist Bane.', 2012,'the_dark_knight_rises',16,0.50,'https://www.youtube-nocookie.com/embed/z5Humz3ONgk?rel=0&amp;showinfo=0'),
		(20, 'Inception',148,'A thief, who steals corporate secrets through use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.',2010,'inception',NULL,1.50,'https://www.youtube-nocookie.com/embed/YoHD9XEInc0?rel=0&amp;showinfo=0'),
		(21, 'Black Swan',108,'A committed dancer wins the lead role in a production of Tchaikovsky''s "Swan Lake" only to find herself struggling to maintain her sanity.',2010,'black_swan',NULL,1.50,'https://www.youtube-nocookie.com/embed/coVi98u5RL0?rel=0&amp;showinfo=0'),
		(22, 'Bad Moms',100, 'When three overworked and under-appreciated moms are pushed beyond their limits, they ditch their conventional responsibilities for a jolt of long overdue freedom, fun, and comedic self-indulgence.',2016,'bad_moms',NULL,1.50,'https://www.youtube-nocookie.com/embed/P0FNjPsANGk?rel=0&amp;showinfo=0'),
		(23, 'Deadpool', 108,'A fast-talking mercenary with a morbid sense of humor is subjected to a rogue experiment that leaves him with accelerated healing powers and a quest for revenge.',2016,'deadpool',NULL,1.50,'https://www.youtube-nocookie.com/embed/FyKWUTwSYAs?rel=0&amp;showinfo=0'),
		(24, 'Doctor Strange',115,'A former neurosurgeon embarks on a journey of healing only to be drawn into the world of the mystic arts.',2016,'doctor_strange',NULL,1.50,'https://www.youtube-nocookie.com/embed/Lt-U_t2pUHI?rel=0&amp;showinfo=0'),
		(25, 'Fantastic Beasts and Where to Find Them',133,'The adventures of writer Newt Scamander in New York''s secret community of witches and wizards seventy years before Harry Potter reads his book in school.',2016,'fantastic_beast',NULL,1.50,'https://www.youtube-nocookie.com/embed/YdgQj7xcDJo?rel=0&amp;showinfo=0'),
		(26, 'Interstellar',169,'A team of explorers travel through a wormhole in space in an attempt to ensure humanity''s survival.',2014,'interstellar',NULL,1.50,'https://www.youtube-nocookie.com/embed/zSWdZVtXT7E?rel=0&amp;showinfo=0'),
		(27, 'Jason Bourne',123,'The CIA''s most dangerous former operative is drawn out of hiding to uncover more explosive truths about his past.',2016,'jason_bourne',NULL,1.50,'https://www.youtube-nocookie.com/embed/F4gJsKZvqE4?rel=0&amp;showinfo=0'),
		(28, 'Lincoln',150,'As the Civil War continues to rage, America''s president struggles with continuing carnage on the battlefield as he fights with many inside his own cabinet on the decision to emancipate the slaves.',2012,'lincoln',NULL,1.50,'https://www.youtube-nocookie.com/embed/qiSAbAuLhqs?rel=0&amp;showinfo=0'),
		(29, 'Shutter Island',138,'In 1954, a U.S. marshal investigates the disappearance of a murderess who escaped from a hospital for the criminally insane.',2010,'shutter_island',NULL,1.50,'https://www.youtube-nocookie.com/embed/qdPw9x9h5CY?rel=0&amp;showinfo=0'),
		(30, 'The Hobbit: An Unexpected Journey',169,'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.',2012,'the_hobbit',NULL,1.50,'https://www.youtube-nocookie.com/embed/SDnYMbYB-nU?rel=0&amp;showinfo=0'),
		(31, 'The Martian',144,'An astronaut becomes stranded on Mars after his team assume him dead, and must rely on his ingenuity to find a way to signal to Earth that he is alive.',2015,'the_martian',NULL,1.50,'https://www.youtube-nocookie.com/embed/ej3ioOneTy8?rel=0&amp;showinfo=0'),
		(32, 'The Revenant', 156, 'A frontiersman on a fur trading expedition in the 1820s fights for survival after being mauled by a bear and left for dead by members of his own hunting team.', 2015, 'the_revenant', NULL, 1.50, 'https://www.youtube-nocookie.com/embed/LoebZZ8K5N0?rel=0&amp;showinfo=0')

INSERT INTO Movie_Directors
VALUES	(11, 1),(11, 2),
		(12, 1),(12, 2),
		(13,1),(13,2),
		(14,6),
		(15,6),
		(16,12),
		(17,16),
		(18,20),
		(19,12),
		(20,12),
		(21,19),
		(22,33),(22,34),
		(23,37),
		(24,41),
		(25,45),
		(26,12),
		(27,51),
		(28,55),
		(29,59),
		(30,62),
		(31,66),
		(32,68)

INSERT INTO Movie_Cast
VALUES  (11,3,'Neo'),(11,4,'Morpheus'),(11,5,'Trinity'),
		(12,3,'Neo'),(12,4,'Morpheus'),(12,5,'Trinity'),
		(13,3,'Neo'),(13,4,'Morpheus'),(13,5,'Trinity'),
		(14,7,'Don Vito Corleone'),(14,8,'Michael Corleone'),(14,9,'Sonny Corleone'),
		(15,8,'Michael Corleone'),(15,10,'Vito Corleone'),(15,11,'Tom Hagen'),
		(16,13,'Bruce Wayne'),(16,14,'Joker'),(16,15,'Harvey Dent'),
		(17,17,'Juror 8'),(17,18,'Juror 3'),(17,19,'Juror 1'),
		(18,21,'Vincent Vega'),(18,22,'Mia Wallace'),(18,23,'Jules Winnfield'),
		(19,13,'Bruce Wayne'),(19,24,'Bane'),(19,25,'Selina'),
		(20,26,'Cobb'),(20,27,'Arthur'),(20,28,'Ariadne'),
		(21,30,'Nina Sayers'),(21,31,'Lily'),(21,32,'Thomas Leroy'),
		(22,31,'Amy'),(22,35,'Carla'),(22,36,'Kiki'),
		(23,38,'Wade'),(23,39,'Vanessa'),(23,40,'Weasel'),
		(24,42,'Dr. Stephen Strange'),(24,43,'Mordo'),(24,44,'Christine Palmer'),
		(25,46,'Newt'),(25,47,'Tina'),(25,48,'Queenie'),
		(26,49,'Cooper'),(26,25,'Brand'),(26,50,'Murph'),
		(27,52,'Jason Bourne'),(27,53,'CIA Director Robert Dewey'),(27,54,'Heather Lee'),
		(28,56,'Abraham Lincoln'),(28,57,'Mary Todd Lincoln'),(28,58,'William Seward'),
		(29,26,'Teddy Daniels'),(29,60,'Rachel 1'),(29,61,'Chuck Aule'),
		(30,63,'Bilbo'),(30,64,'Gandalf'),(30,65,'Thorin'),
		(31,52,'Mark Watney'),(31,50,'Melissa Lewis'),(31,67,'Annie Montrose'),
		(32,26,'Hugh Glass'),(32,24,'John Fitzgerald'),(32,69,'Bridger')

INSERT INTO Genre
VALUES  ('Action'),
		('Adventure'),
		('Animation'),
		('Biography'),
		('Comedy'),
		('Crime'),
		('Documentary'),
		('Drama'),
		('Family'),
		('Fantasy'),
		('Film-Noir'),
		('History'),
		('Horror'),
		('Music'),
		('Musical'),
		('Mystery'),
		('Romance'),
		('Sci-Fi'),
		('Sport'),
		('Thriller'),
		('War'),
		('Western')

INSERT INTO Movie_Genre
VALUES	(11, 'Action'),(11, 'Sci-Fi'),
		(12, 'Action'),(12, 'Sci-Fi'),
		(13, 'Action'),(13, 'Sci-Fi'),
		(14, 'Crime'),(14, 'Drama'),
		(15, 'Crime'),(15, 'Drama'),
		(16, 'Action'),(16, 'Crime'),(16, 'Drama'),
		(17, 'Crime'),(17, 'Drama'),
		(18, 'Crime'),(18, 'Drama'),
		(19,'Action'),(19,'Thriller'),
		(20,'Action'),(20,'Adventure'),(20,'Sci-Fi'),
		(21,'Drama'),(21,'Thriller'),
		(22,'Comedy'),
		(23,'Action'),(23,'Adventure'),(23, 'Comedy'),
		(24,'Action'),(24,'Adventure'),(24, 'Fantasy'),
		(25,'Adventure'),(25,'Family'),(25,'Fantasy'),
		(26,'Adventure'),(26,'Drama'),(26,'Sci-Fi'),
		(27,'Action'),(27,'Thriller'),
		(28,'Biography'),(28,'Drama'),(28,'History'),
		(29,'Mystery'),(29,'Thriller'),
		(30,'Adventure'),(30,'Fantasy'),
		(31,'Adventure'),(31,'Drama'),(31,'Sci-Fi'),
		(32,'Adventure'),(32,'Drama'),(32,'Thriller')

INSERT INTO Country
VALUES ('The Netherlands'),
	   ('Belgium'),
	   ('United Kingdom'),
	   ('United States')

SELECT * FROM Movie
SELECT * FROM Person
SELECT * FROM Movie_Cast
SELECT * FROM Movie_Directors
SELECT * FROM Genre
SELECT * FROM Movie_Genre
SELECT * FROM Country
SELECT * FROM Customer
SELECT * FROM WatchHistory